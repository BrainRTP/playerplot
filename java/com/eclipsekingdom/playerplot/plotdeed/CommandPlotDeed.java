package com.eclipsekingdom.playerplot.plotdeed;

import com.eclipsekingdom.playerplot.Permissions;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandPlotDeed implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Permissions.canSummonPlotDeed(player)){
                int amt = 1;
                if(args.length > 0){
                    try{
                        amt = Integer.parseInt(args[0]);
                    }catch (Exception e){
                        //do nothing
                    }
                }
                ItemStack deed = new PlotDeed().asItem();
                deed.setAmount(amt);
                player.getInventory().addItem(deed);
            }else{
                Notifications.sendWarning(player, "You do not have permission for this command");
            }
        }

        return false;
    }



}
