package com.eclipsekingdom.playerplot.plotdeed;

import com.eclipsekingdom.playerplot.util.ItemOperation;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class PlotDeed {

    public PlotDeed(){
        this.itemStack = buildItemStack();
    }

    public ItemStack asItem(){
        return itemStack.clone();
    }

    public static boolean isPlotDeed(ItemStack itemStack){
        if(ItemOperation.hasLoreID(itemStack)){
            return LORE_ID.equals(ItemOperation.getLoreID(itemStack));
        }else{
            return false;
        }
    }

    public static PlotDeedTheme getRandomTheme(){
        int index = random.nextInt(PLOT_DEED_TYPE_LIST.size());
        return PLOT_DEED_TYPE_LIST.get(index);
    }

    private final ItemStack itemStack;


    private static final String LORE_ID = ChatColor.DARK_GRAY + "a serious looking piece of paper";
    private static final List<PlotDeedTheme> PLOT_DEED_TYPE_LIST = Collections.unmodifiableList(Arrays.asList(PlotDeedTheme.values()));
    private static final Random random = new Random();

    private ItemStack buildItemStack(){
        ItemStack plotDeed = new ItemStack(Material.PAPER);
        ItemMeta meta = plotDeed.getItemMeta();

        //create meta
        PlotDeedTheme theme = getRandomTheme();
        meta.setDisplayName(theme.title);
        List<String> lore = new ArrayList<>();
        lore.add(LORE_ID);
        lore.add(ChatColor.RED + "1 use only - click to activate");
        lore.add(ChatColor.GRAY + "+1 plot");
        meta.setLore(lore);
        meta.setCustomModelData(theme.model);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        //meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        meta.addEnchant(Enchantment.DURABILITY,1,true);
        plotDeed.setItemMeta(meta);
        return plotDeed;
    }


}
