package com.eclipsekingdom.playerplot.plotdeed;

import org.bukkit.ChatColor;

public enum  PlotDeedTheme {
    SWAMP(ChatColor.GREEN + "Swamp Plot Deed",1),
    MOUNTAIN(ChatColor.RED + "Mountain Plot Deed",2),
    OCEAN(ChatColor.BLUE + "Ocean Plot Deed",3);


    PlotDeedTheme(String title, int model) {
        this.title = title;
        this.model = model;
    }

    public final String title;
    public final int model;

}
