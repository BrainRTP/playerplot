package com.eclipsekingdom.playerplot.plotdeed;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.Permissions;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlotDeedListener implements Listener {

    private UserCache userCache;
    private PluginConfig pluginConfig;

    public PlotDeedListener(PlayerPlot plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.userCache = plugin.getUserCache();
        this.pluginConfig = plugin.getPluginConfig();
    }

    @EventHandler
    public void onClick(PlayerInteractEvent e){
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR){
            if(PlotDeed.isPlotDeed(e.getItem())){
                e.setCancelled(true);
                Player player = e.getPlayer();
                UserData userData = userCache.getData(player.getUniqueId());
                int availablePlots = pluginConfig.getStartingPlotNum() + userData.getBonusPlots();
                if( (availablePlots < pluginConfig.getMaxPlotNum()) || Permissions.hasNoLimit(player) ){
                    userData.grantBonusPlot();
                    Notifications.sendSuccess(player, "+1 plot");
                    player.playSound(player.getLocation(), Sound.ITEM_BOOK_PAGE_TURN, 1f, 1f);
                    consumeItem(e.getItem());
                }else{
                    Notifications.sendWarning(player, "You already have the maximum number of plots");
                }

            }
        }
    }

    private void consumeItem(ItemStack itemStack){
        itemStack.setAmount(itemStack.getAmount() - 1);
    }

}