package com.eclipsekingdom.playerplot.plot;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.Permissions;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.raid.ActiveRaids;
import com.eclipsekingdom.playerplot.util.Notifications;
import com.google.common.collect.ImmutableSet;
import net.minecraft.server.v1_14_R1.EntityTypes;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class PlotProtection implements Listener {

    private ActiveRaids activeRaids = ActiveRaids.getInstance();
    private PlotCache plotCache;
    public static final String plotAttackKey = "plotAttackKey";

    public PlotProtection(PlayerPlot plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plotCache = plugin.getPlotCache();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBuild(BlockPlaceEvent e){
        Player builder = e.getPlayer();
        if(!canBuild(builder, e.getBlock().getLocation())){
            e.setCancelled(true);
            e.getPlayer().sendMessage(PROTECTED_WARNING);
        }
    }

    private static final String PROTECTED_WARNING = Notifications.themeDark+"[PlayerPlot]" + ChatColor.RED + " This region is protected";

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPiston(BlockPistonExtendEvent e){
        Plot pistonPlot = plotCache.getPlot(e.getBlock().getLocation());
        Vector direction = e.getDirection().getDirection();
        for(Block block: e.getBlocks()){
            Plot blockPlot = plotCache.getPlot(block.getLocation().add(direction.getX(), 0, direction.getZ()));
            if(blockPlot != pistonPlot){
                e.setCancelled(true);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onFlow(BlockFromToEvent e){
        Plot plotTo = plotCache.getPlot(e.getToBlock().getLocation());
        if(plotTo != null){
            Plot plotFrom = plotCache.getPlot(e.getBlock().getLocation());
            if(plotTo != plotFrom){
                e.setCancelled(true);
            }
        }
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onBreak(BlockBreakEvent e){
        if(e.getPlayer() != null){
            Player breaker = e.getPlayer();
            if(!canBuild(breaker, e.getBlock().getLocation())){
                e.setCancelled(true);
                breaker.sendMessage(PROTECTED_WARNING);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInteractAt(PlayerInteractAtEntityEvent e){
        if(interactableAtEntities.contains(e.getRightClicked().getType())){
            if(!canBuild(e.getPlayer(), e.getRightClicked().getLocation())){
                e.setCancelled(true);
                e.getPlayer().sendMessage(PROTECTED_WARNING);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInteractEntity(PlayerInteractEntityEvent e){
        if(interactableEntities.contains(e.getRightClicked().getType())){
            if(!canBuild(e.getPlayer(), e.getRightClicked().getLocation())){
                e.setCancelled(true);
                e.getPlayer().sendMessage(PROTECTED_WARNING);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInteract(PlayerInteractEvent e){
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
            Block block  = e.getClickedBlock();
            Player player = e.getPlayer();
            if(!canBuild(player, block.getLocation())){
                Material material = block.getType();
                if(interactables.contains(material)
                        || isStripLogAttemp(e.getItem(), material)
                        || hasInteractableTag(material)
                        || (e.getItem() != null && placeables.contains(e.getItem().getType()))){
                    e.setCancelled(true);
                    player.sendMessage(PROTECTED_WARNING);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDestroy(HangingBreakByEntityEvent e){
        if (e.getRemover() instanceof Player) {
            Player player = (Player)e.getRemover();
            if(!canBuild(player, e.getEntity().getLocation())){
                e.setCancelled(true);
                player.sendMessage(PROTECTED_WARNING);
            }
        }
    }

    private boolean isStripLogAttemp(ItemStack itemStack, Material material){
        return (axes.contains(itemStack) && Tag.LOGS.isTagged(material));
    }

    private Set<Material> axes = ImmutableSet.<Material>builder()
            .add(Material.WOODEN_AXE)
            .add(Material.STONE_AXE)
            .add(Material.IRON_AXE)
            .add(Material.GOLDEN_AXE)
            .add(Material.DIAMOND_AXE)
            .build();

    private boolean hasInteractableTag(Material material){
        for(Tag tag: interactableTags){
            if(tag.isTagged(material)){
                return true;
            }
        }
        return false;
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onBlockExplode(BlockExplodeEvent e){
        List<Block> blockList = e.blockList();
        for(int i=blockList.size() -1; i>=0; i--){
            if(plotCache.getPlot(blockList.get(i).getLocation()) != null){
                blockList.remove(i);
            }
        }
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onEntityExplode(EntityExplodeEvent e){
        List<Block> blockList = e.blockList();
        for(int i=blockList.size() -1; i>=0; i--){
            if(plotCache.getPlot(blockList.get(i).getLocation()) != null){
                blockList.remove(i);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onFire(BlockIgniteEvent e){
        if(plotCache.getPlot(e.getBlock().getLocation()) != null){
            if(e.getCause() == BlockIgniteEvent.IgniteCause.SPREAD){
                e.setCancelled(true);
            }
        }
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onPlayerInteract(PlayerInteractEvent e)  {
        if (e.getAction() == Action.PHYSICAL) {
            Block block = e.getClickedBlock();
            if(block.getType() == Material.FARMLAND){
                if(!canBuild(e.getPlayer(), block.getLocation())){
                    e.setCancelled(true);
                    e.getPlayer().sendMessage(PROTECTED_WARNING);
                }
            }
        }
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onEntityInteract(EntityInteractEvent e)  {
        if(!(e.getEntity() instanceof Villager)){
            Block block = e.getBlock();
            if(block.getType() == Material.FARMLAND){
                if(plotCache.getPlot(block.getLocation()) != null){
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onHurt(EntityDamageByEntityEvent e)  {
        if(e.getEntity().hasMetadata(plotAttackKey)) return;
        if(!(e.getEntity() instanceof Player || e.getEntity() instanceof Monster)){
            if(wasPlayer(e)){
                Player damager = getPlayer(e);
                if(!canBuild(damager, e.getEntity().getLocation())){
                    e.setCancelled(true);
                    damager.sendMessage(PROTECTED_WARNING);

                }
            }
        }
    }

    private boolean wasPlayer(EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player){
            return true;
        }else if(e.getDamager() instanceof Projectile){
            Projectile projectile = (Projectile) e.getDamager();
            if(projectile.getShooter() instanceof  Player){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    private Player getPlayer(EntityDamageByEntityEvent e){
        if(e.getDamager() instanceof Player){
            return (Player) e.getDamager();
        }else if(e.getDamager() instanceof Projectile){
            Projectile projectile = (Projectile) e.getDamager();
            if(projectile.getShooter() instanceof  Player){
                return (Player) projectile.getShooter();
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    private static List<Material> interactables = Arrays.asList(
            Material.ACACIA_FENCE_GATE,
            Material.BIRCH_FENCE_GATE,
            Material.DARK_OAK_FENCE_GATE,
            Material.SPRUCE_FENCE_GATE,
            Material.JUNGLE_FENCE_GATE,
            Material.OAK_FENCE_GATE,
            Material.ANVIL,
            Material.BEACON,
            Material.BREWING_STAND,
            Material.COMMAND_BLOCK,
            Material.LOOM,
            Material.CHEST,
            Material.DAYLIGHT_DETECTOR,
            Material.DISPENSER,
            Material.DROPPER,
            Material.ENCHANTING_TABLE,
            Material.ENDER_CHEST,
            Material.FURNACE,
            Material.HOPPER,
            Material.HOPPER_MINECART,
            Material.ITEM_FRAME,
            Material.LEVER,
            Material.MINECART,
            Material.NOTE_BLOCK,
            Material.COMPARATOR,
            Material.TRAPPED_CHEST,
            Material.SMOKER,
            Material.BARREL,
            Material.SHULKER_BOX,
            Material.BLACK_SHULKER_BOX,
            Material.BLUE_SHULKER_BOX,
            Material.BROWN_SHULKER_BOX,
            Material.CYAN_SHULKER_BOX,
            Material.GRAY_SHULKER_BOX,
            Material.GREEN_SHULKER_BOX,
            Material.LIGHT_BLUE_SHULKER_BOX,
            Material.LIGHT_GRAY_SHULKER_BOX,
            Material.LIME_SHULKER_BOX,
            Material.MAGENTA_SHULKER_BOX,
            Material.ORANGE_SHULKER_BOX,
            Material.PINK_SHULKER_BOX,
            Material.PURPLE_SHULKER_BOX,
            Material.RED_SHULKER_BOX,
            Material.WHITE_SHULKER_BOX,
            Material.YELLOW_SHULKER_BOX,
            Material.YELLOW_SHULKER_BOX,
            Material.SPAWNER
    );

    private static List<EntityType> interactableAtEntities = Arrays.asList(
            EntityType.ARMOR_STAND
    );

    private static List<EntityType> interactableEntities = Arrays.asList(
            EntityType.ITEM_FRAME,
            EntityType.VILLAGER,
            EntityType.LEASH_HITCH
    );

    private static List<Material> placeables = Arrays.asList(
            Material.ITEM_FRAME,
            Material.PAINTING
    );

    private static List<Tag> interactableTags = Arrays.asList(Tag.BEDS, Tag.DOORS, Tag.BUTTONS, Tag.TRAPDOORS, Tag.WALL_SIGNS);


    private boolean canBuild(Player player, Location location){
        Plot plot = plotCache.getPlot(location);
        if(plot != null){
            if(Permissions.canBuildEverywhere(player) || plot.getOwnerID().equals(player.getUniqueId()) || plot.trusts(player)){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

}
