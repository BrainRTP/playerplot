package com.eclipsekingdom.playerplot.plot;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.admin.ActionActivity;
import com.eclipsekingdom.playerplot.plot.actions.*;
import com.eclipsekingdom.playerplot.plot.actions.Error;
import com.eclipsekingdom.playerplot.util.PluginHelp;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CommandPlayerPlot implements CommandExecutor {

    private PlayerPlot plugin;
    private PluginHelp pluginHelp;

    public CommandPlayerPlot(PlayerPlot plugin){
        this.plugin = plugin;
        this.pluginHelp = new PluginHelp(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {


        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (args.length == 0) {
                pluginHelp.showTo(player);
            } else {
                switch (args[0].toLowerCase()){
                    case "scan": new Scan(plugin).run(player, args); break;
                    case "help": pluginHelp.showTo(player); break;
                    case "info": new Info(plugin).run(player, args); break;
                    case "claim": new Claim(plugin).run(player, args); break;
                    case "free": new Free(plugin).run(player, args); break;
                    case "list": new PList(plugin).run(player, args); break;
                    case "trust": new AddFriend(plugin).run(player, args); break;
                    case "untrust": new RemFriend(plugin).run(player, args); break;
                    case "upgrade": new Upgrade(plugin).run(player, args); break;
                    case "downgrade": new Downgrade(plugin).run(player, args); break;
                    case "rename": new Rename(plugin).run(player, args); break;
                    case "activity": new ActionActivity(plugin).run(player, args); break;
                    default: new Error().run(player, args); break;
                }
            }
        }


        return false;
    }

}
