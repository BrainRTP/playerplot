package com.eclipsekingdom.playerplot.plot;

import org.bukkit.World;
import org.bukkit.Location;

public class PlotPoint {

    private final int x;
    private final int z;

    public PlotPoint(int x, int z){
        this.x = x;
        this.z = z;
    }

    public static final PlotPoint getMaxCornerFromCenter(PlotPoint centerPoint, int sideLength){

        int halfLength;
        if(sideLength%2 == 0){
            halfLength = sideLength/2;
        }else{
            halfLength = (sideLength-1)/2;
        }
        return new PlotPoint(centerPoint.getX() + halfLength, centerPoint.getZ() + halfLength);
    }

    public static final PlotPoint getMinCornerFromCenter(PlotPoint centerPoint, int sideLength){
        int halfLength;
        if(sideLength%2 == 0){
            halfLength = sideLength/2;
            halfLength--;
        }else{
            halfLength = (sideLength-1)/2;
        }
        return new PlotPoint(centerPoint.getX() - halfLength, centerPoint.getZ() - halfLength);
    }

    public static final PlotPoint[] getCornersFromLocation(PlotPoint centerPoint, int sideLength){
        PlotPoint[] corners = new PlotPoint[4];
        corners[0] = getMinCornerFromCenter(centerPoint, sideLength); //bottom left
        corners[1] = getMaxCornerFromCenter(centerPoint, sideLength);// top right
        corners[2] = new PlotPoint(corners[0].getX(), corners[1].getZ());//top left
        corners[3] = new PlotPoint(corners[1].getX(), corners[0].getZ());//bottom right
        return corners;
    }

    public Location asLocation(World world){
        return new Location(world, x, 70, z, 0,0);
    }

    public static final PlotPoint fromLocation(Location location){
        return new PlotPoint(location.getBlockX(), location.getBlockZ());
    }

    public int getX(){
        return x;
    }

    public int getZ() {
        return z;
    }


}
