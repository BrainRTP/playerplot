package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.PlotPoint;
import com.eclipsekingdom.playerplot.util.Notifications;
import net.minecraft.server.v1_14_R1.PacketPlayOutWorldBorder;
import net.minecraft.server.v1_14_R1.WorldBorder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Scan implements PlotAction {

    private PlayerPlot plugin;
    private PlotCache plotCache;

    public Scan(PlayerPlot plugin){
        this.plugin = plugin;
        this.plotCache = plugin.getPlotCache();
    }

    public String PLOT_STRING(String plotName, String ownerName){
        return (Notifications.themeDark + plotName + " ~ " + ownerName + " ~");
    }

    public static void showPlot(Player player, Plot plot, PlayerPlot plugin, int duration){
        WorldBorder worldBorder = new WorldBorder();
        worldBorder.world = ((CraftWorld) player.getWorld()).getHandle();
        PlotPoint center = plot.calculatePlotCenter();
        int length = plot.getMaxCorner().getX() - plot.getMinCorner().getX();
        if(length %2 ==0){
            worldBorder.setCenter(center.getX()+0.5, center.getZ()+0.5);
            worldBorder.setSize(length);
        }else{
            worldBorder.setCenter(center.getX()+1, center.getZ()+1);
            worldBorder.setSize(length);
        }
        worldBorder.setDamageAmount(0);
        PacketPlayOutWorldBorder packetPlayOutWorldBorder = new PacketPlayOutWorldBorder(worldBorder, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE);

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutWorldBorder);

        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                WorldBorder worldBorder = new WorldBorder();
                worldBorder.world = ((CraftWorld) player.getWorld()).getHandle();
                PacketPlayOutWorldBorder packetPlayOutWorldBorder = new PacketPlayOutWorldBorder(worldBorder, PacketPlayOutWorldBorder.EnumWorldBorderAction.LERP_SIZE);
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutWorldBorder);
            }
        },20*duration);
    }

    @Override
    public void run(Player player, String[] args) {
        Plot plot = plotCache.getPlot(player.getLocation());
        if(plot != null){
            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_POWER_SELECT, 1f, 0.77f);
            player.sendMessage(Notifications.themeLight + "[PlayerPlot] " + PLOT_STRING(plot.getName(), plot.getOwnerName()));
            showPlot(player, plot, plugin, 5);
        }else{
            player.sendMessage(Notifications.themeDark + "[PlayerPlot]: " + ChatColor.RED + "No plots detected");
        }
    }
}
