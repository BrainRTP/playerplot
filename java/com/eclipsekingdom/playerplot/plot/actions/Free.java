package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class Free extends StandingPlotAction {

    private PlayerPlot plugin;
    private PlotCache plotCache;
    private UserCache userCache;
    public Free(PlayerPlot plugin){
        super(plugin);
        this.plugin = plugin;
        this.plotCache = plugin.getPlotCache();
        this.userCache = plugin.getUserCache();
    }

    @Override
    protected void executeAction(Player player, Plot plot, String[] args){
        Scan.showPlot(player, plot, plugin, 1);
        plotCache.removePlot(plot);
        UserData userData = userCache.getData(player.getUniqueId());
        userData.removePlot(plot);
        player.sendMessage(SUCCESSFUL_DELETE_MESSAGE(plot.getName()));
        player.playSound(player.getLocation(), Sound.BLOCK_BEACON_DEACTIVATE, 1f, 1f);
    }

    private static final String SUCCESSFUL_DELETE_MESSAGE(String plotName){
        return (
                Notifications.themeLight + "Plot "
                + Notifications.themeDark + plotName
                + Notifications.themeLight + " was deleted"
        );
    }

}
