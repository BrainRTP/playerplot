package com.eclipsekingdom.playerplot.plot.actions;

import org.bukkit.entity.Player;

public interface PlotAction {

    void run(Player player, String[] args);

}
