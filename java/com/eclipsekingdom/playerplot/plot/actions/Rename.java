package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.validation.NameStatus;
import com.eclipsekingdom.playerplot.plot.validation.NameValidation;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.entity.Player;

public class Rename extends StandingPlotAction {

    private PlotCache plotCache;
    private UserCache userCache;

    public Rename(PlayerPlot plugin){
        super(plugin);
        this.plotCache = plugin.getPlotCache();
        this.userCache = plugin.getUserCache();
    }

    @Override
    protected void executeAction(Player player, Plot plot, String[] args){
        if(args.length > 1){
            String newName = args[1];
            UserData userData = userCache.getData(player.getUniqueId());
            NameStatus nameStatus = NameValidation.clean(newName, userData);
            if(nameStatus == NameStatus.VALID){
                plot.setName(newName);
                plotCache.reportPlotModification(plot);
                player.sendMessage(SUCCESSFUL_RENAME_MESSAGE(newName));
            }else{
                Notifications.sendWarning(player, nameStatus.getMessage());
            }
        }else{
            Notifications.sendWarning(player, "Please provide a new name");
        }
    }

    private static final String SUCCESSFUL_RENAME_MESSAGE(String newName){
        return (Notifications.themeLight + "Plot successfully renamed to " + Notifications.themeDark + newName);
    }


}
