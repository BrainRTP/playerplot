package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.entity.Player;

public class Error implements PlotAction {
    @Override
    public void run(Player player, String[] args) {
        Notifications.sendWarning(player, ERROR);
        Notifications.sendTip(player,"plot help", "for help");
    }


    private static final String ERROR = "Unrecognized command";
}
