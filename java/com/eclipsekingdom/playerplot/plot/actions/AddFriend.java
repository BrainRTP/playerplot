package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.plot.Friend;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class AddFriend extends StandingPlotAction {

    private PlotCache plotCache;

    public AddFriend(PlayerPlot plugin){
        super(plugin);
        this.plotCache = plugin.getPlotCache();
    }

    @Override
    protected void executeAction(Player player, Plot plot, String[] args) {
        if(args.length > 1){
            String targetName = args[1];
            Player pFriend = getPlayer(targetName);
            if(pFriend != null){
                processFriendRequest(player, plot, new Friend(pFriend.getUniqueId(), pFriend.getName()));
            }else{
                OfflinePlayer opFriend = getOfflinePlayer(targetName);
                if(opFriend != null){
                    processFriendRequest(player, plot, new Friend(opFriend.getUniqueId(), opFriend.getName()));
                }else{
                    Notifications.sendNotFound(player, "Player", targetName);
                }
            }
        }else{
            Notifications.sendFormat(player, "plot trust [player]");
        }
    }


    private static final String ADD_SELF_ERROR = "You are already your own best friend!";
    private static final String ALREADY_FRIEND_ERROR(String friendName, String plotName){
        return (Notifications.themeDark + friendName+ ChatColor.RED + " is already added to "+ Notifications.themeDark + plotName);
    }
    private static final String SUCCESSFUL_ADD_MESSAGE(String friendName, String plotName){
        return (Notifications.themeDark + friendName+ Notifications.themeLight + " was added to "+ Notifications.themeDark + plotName);
    }

    private static final String FRIEND_MESSAGE(String ownerName, String plotName){
        return (Notifications.themeDark + ownerName + Notifications.themeLight + " has granted you access to the plot " + Notifications.themeDark + plotName);
    }

    private Player getPlayer(String name){
        Player player = null;
        for(Player p: Bukkit.getOnlinePlayers()){
            if(p.getName().equalsIgnoreCase(name)){
                player = p;
            }
        }
        return player;
    }

    private OfflinePlayer getOfflinePlayer(String name){
        OfflinePlayer oPlayer = null;
        for(OfflinePlayer op: Bukkit.getOfflinePlayers()){
            if(op.getName().equalsIgnoreCase(name)){
                oPlayer = op;
            }
        }
        return oPlayer;
    }

    private void processFriendRequest(Player player, Plot plot, Friend friend){
        if(!plot.getFriends().contains(friend)){
            if(!plot.getOwnerID().equals(friend.getUuid())){
                plot.addFriend(friend);
                plotCache.reportPlotModification(plot);
                player.sendMessage(SUCCESSFUL_ADD_MESSAGE(friend.getName(), plot.getName()));
                notifyFriend(player, plot, friend);
            }else{
                Notifications.sendWarning(player, ADD_SELF_ERROR);
            }
        }else{
            Notifications.sendWarning(player, ALREADY_FRIEND_ERROR(friend.getName(), plot.getName()));
        }
    }

    private void notifyFriend(Player owner, Plot plot, Friend friend){
        Player friendPlayer = Bukkit.getServer().getPlayer(friend.getUuid());
        if(friendPlayer != null){
            if(friendPlayer.isOnline()){
                friendPlayer.sendMessage(FRIEND_MESSAGE(owner.getName(), plot.getName()));
            }
        }
    }

}


