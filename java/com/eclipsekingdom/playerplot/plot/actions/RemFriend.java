package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class RemFriend extends StandingPlotAction {

    private PlotCache plotCache;

    public RemFriend(PlayerPlot plugin){
        super(plugin);
        this.plotCache = plugin.getPlotCache();
    }

    @Override
    protected void executeAction(Player player, Plot plot, String[] args){
        if(args.length > 1){
            String friendName = args[1];
            if(plot.trusts(friendName)){
                plot.removeFriend(friendName);
                plotCache.reportPlotModification(plot);
                player.sendMessage(SUCCESSFUL_REMOVE_MESSAGE(friendName, plot.getName()));
            }else{
                Notifications.sendWarning(player, NOT_FRIEND_ERROR(friendName, plot.getName()));
            }
        }else{
            Notifications.sendFormat(player, "plot trust [player]");
        }
    }

    private static final String NOT_FRIEND_ERROR(String friendName, String plotName){
        return (Notifications.themeDark + friendName+ ChatColor.RED + " was not added to "+ Notifications.themeDark + plotName);
    }
    private static final String SUCCESSFUL_REMOVE_MESSAGE(String friendName, String plotName){
        return (Notifications.themeDark + friendName + Notifications.themeLight + " was removed from " + Notifications.themeDark + plotName);
    }

}
