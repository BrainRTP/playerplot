package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.actions.PlotAction;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.entity.Player;

public abstract class StandingPlotAction implements PlotAction {

    private PlotCache plotCache;

    public StandingPlotAction(PlayerPlot plugin){
        this.plotCache = plugin.getPlotCache();
    }

    @Override
    public void run(Player player, String[] args) {
        Plot requestedPlot = plotCache.getPlot(player.getLocation());
        if(requestedPlot != null){
            if(requestedPlot.getOwnerID().equals(player.getUniqueId())){
                executeAction(player, requestedPlot, args);
            }else{
                Notifications.sendWarning(player, NOT_OWNER_ERROR);
            }
        }else{
            Notifications.sendWarning(player, NOT_STANDING_IN_PLOT);
        }
    }

    protected abstract void executeAction(Player player, Plot plot, String[] args);

    private static final String NOT_STANDING_IN_PLOT = "You are not standing in a plot";
    private static final String NOT_OWNER_ERROR = "You are not the owner of this plot";

}
