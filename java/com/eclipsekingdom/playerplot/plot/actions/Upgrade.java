package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.PlotPoint;
import com.eclipsekingdom.playerplot.plot.validation.RegionStatus;
import com.eclipsekingdom.playerplot.plot.validation.RegionValidation;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class Upgrade extends StandingPlotAction {

    private PlayerPlot plugin;
    private PlotCache plotCache;
    private UserCache userCache;
    private PluginConfig pluginConfig;
    private RegionValidation regionValidation;
    private int unitSideLength;

    public Upgrade(PlayerPlot plugin){
        super(plugin);
        this.plugin = plugin;
        this.plotCache = plugin.getPlotCache();
        this.userCache = plugin.getUserCache();
        this.pluginConfig = plugin.getPluginConfig();
        this.regionValidation = new RegionValidation(plugin);
        this.unitSideLength = pluginConfig.getPlotUnitSideLength();
    }

    @Override
    protected void executeAction(Player player, Plot plot, String[] args){
        UserData userData = userCache.getData(player.getUniqueId());
        int plotsUsed = userData.getPlotsUsed();
        int plotsAvailable = pluginConfig.getStartingPlotNum() + userData.getBonusPlots();
        if(plotsUsed < plotsAvailable){
            PlotPoint center = plot.calculatePlotCenter();
            int newSideLength = getUpgradeLength(plot.getRelativeSize());
            RegionStatus regionStatus = regionValidation.canPlotBeUpgradedAt(plot.getWorld(), center, newSideLength);
            if(regionStatus == RegionStatus.VALID){

                Scan.showPlot(player, plot, plugin,1);
                player.playSound(player.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 1, 1);

                PlotPoint newMin = PlotPoint.getMinCornerFromCenter(center, newSideLength);
                PlotPoint newMax = PlotPoint.getMaxCornerFromCenter(center, newSideLength);
                plot.setRegion(newMin, newMax);
                plot.incrementRelativeSize();
                plotCache.reportPlotModification(plot);

                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        Scan.showPlot(player, plot, plugin,7);
                        player.sendMessage(SUCCESSFUL_UPGRADE(plot.getName()));
                    }
                }, (20 * 1) + 3);

            }else{
                Notifications.sendWarning(player, regionStatus.getMessage());
            }
        }else{
            Notifications.sendWarning(player, PLOT_LIMIT_ERROR);
            Notifications.sendTip(player, "/plot free", "to remove a plot");
            Notifications.sendTip(player,"/plot list", "to view all your plots");
        }
    }


    private static final String PLOT_LIMIT_ERROR = "Plot limit reached";
    private static final String SUCCESSFUL_UPGRADE(String plotName){
        return (Notifications.themeDark + plotName + Notifications.themeLight + " was upgraded");
    }

    public int getUpgradeLength(int relSize){
        relSize++;
        int newLength;
        int unitSquared = unitSideLength * unitSideLength;
        newLength = (int)Math.round(Math.sqrt(Math.round(unitSquared * relSize)));
        return newLength;
    }



}
