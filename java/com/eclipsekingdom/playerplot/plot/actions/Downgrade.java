package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.PlotPoint;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class Downgrade extends StandingPlotAction {

    private PlayerPlot plugin;
    private PlotCache plotCache;
    private PluginConfig pluginConfig;
    private int unitSideLength;

    public Downgrade(PlayerPlot plugin){
        super(plugin);
        this.plugin = plugin;
        this.pluginConfig = plugin.getPluginConfig();
        this.plotCache = plugin.getPlotCache();
        this.unitSideLength = pluginConfig.getPlotUnitSideLength();
    }

    @Override
    protected void executeAction(Player player, Plot plot, String[] args) {
        PlotPoint center = plot.calculatePlotCenter();
        int newSideLength = getDowngradeLength(plot.getRelativeSize());
        if(newSideLength >= unitSideLength){

            Scan.showPlot(player, plot, plugin,1);
            player.playSound(player.getLocation(), Sound.BLOCK_BEACON_DEACTIVATE, 1f, 1f);

            PlotPoint newMin = PlotPoint.getMinCornerFromCenter(center, newSideLength);
            PlotPoint newMax = PlotPoint.getMaxCornerFromCenter(center, newSideLength);
            plot.setRegion(newMin, newMax);
            plot.decrementRelativeSize();
            plotCache.reportPlotModification(plot);

            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                @Override
                public void run() {
                    Scan.showPlot(player, plot, plugin,7);
                    player.sendMessage(SUCCESSFUL_DOWNGRADE(plot.getName()));
                }
            }, (20 * 1) + 3);

        }else{
            Notifications.sendWarning(player, SMALL_PLOT_ERROR(plot.getName()));
        }
    }

    private static final String SMALL_PLOT_ERROR(String plotName){
        return (Notifications.themeDark + plotName+ ChatColor.RED + " can not be downgraded any further");
    }
    private static final String SUCCESSFUL_DOWNGRADE(String plotName){
        return (Notifications.themeDark + plotName+ Notifications.themeLight + " was downgraded");
    }

    public int getDowngradeLength(int relSize){
        relSize--;
        int newLength;
        int unitSquared = unitSideLength * unitSideLength;
        newLength = (int)Math.round(Math.sqrt(Math.round(unitSquared * relSize)));
        return newLength;
    }

}
