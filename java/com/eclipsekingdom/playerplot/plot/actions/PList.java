package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.PlotPoint;
import com.eclipsekingdom.playerplot.util.InfoList;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PList implements PlotAction {

    private UserCache userCache;
    private PluginConfig pluginConfig;

    public PList(PlayerPlot plugin){
        this.userCache = plugin.getUserCache();
        this.pluginConfig = plugin.getPluginConfig();
    }

    @Override
    public void run(Player player, String[] args) {
        UserData userData = userCache.getData(player.getUniqueId());
        List<String> items = new ArrayList<>();
        for(Plot plot: userData.getPlots()){
            items.add(getListString(plot));
        }
        InfoList infoList = new InfoList(LIST_TITLE(userData),items, 7, "plot list");

        if(args.length == 1){ //  /pp PList
            infoList.displayTo(player, 1);
        }else{ //  /pp PList [page]
            int pageNum = 1;
            try{
                pageNum = Integer.parseInt(args[1]);
            }catch (Exception e){}

            infoList.displayTo(player, pageNum);
        }
    }


    private final String LIST_TITLE(UserData userData){
        int plotsUsed = userData.getPlotsUsed();
        int plotsMax = userData.getBonusPlots() + pluginConfig.getStartingPlotNum();
        return (ChatColor.LIGHT_PURPLE + "Your Plots (" + plotsUsed + "/" + plotsMax + "):");
    }

    private String getListString(Plot plot){
        int length = plot.calculateSideLength();
        PlotPoint center = plot.calculatePlotCenter();
        String displayString = ChatColor.DARK_PURPLE + plot.getName() + " ("+length+"x"+length + ") ";
        displayString += (ChatColor.GRAY + "center: (" + center.getX() +","+center.getZ()+")" + " world: " + plot.getWorld().getName());
        return displayString;
    }
}
