package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.plot.Friend;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.PlotPoint;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Info extends StandingPlotAction {

    public Info(PlayerPlot plugin){
        super(plugin);
    }

    @Override
    protected void executeAction(Player player, Plot plot, String[] args) {
        player.sendMessage(Notifications.themeDark + "- - - " + plot.getName() + " - - -");
        PlotPoint minCorner = plot.getMinCorner();
        PlotPoint maxCorner = plot.getMaxCorner();
        int length = maxCorner.getX() - minCorner.getX() + 1;
        player.sendMessage(Notifications.themeLight +"Area: " + ChatColor.RESET + length + " x " + length + "");
        PlotPoint center = plot.calculatePlotCenter();
        player.sendMessage(Notifications.themeLight + "center: " + ChatColor.RESET + "x:" +center.getX() + " z:" + center.getZ());
        player.sendMessage(Notifications.themeLight + "corner (min): " + ChatColor.RESET + "x:" +minCorner.getX() + " z:" + minCorner.getZ());
        player.sendMessage(Notifications.themeLight + "corner (max): " + ChatColor.RESET + "x:" +maxCorner.getX() + " z:" + maxCorner.getZ());
        player.sendMessage(Notifications.themeLight + "world: " + ChatColor.RESET + plot.getWorld().getName());
        player.sendMessage(Notifications.themeLight + "entity: " + ChatColor.RESET + plot.getRelativeSize());
        player.sendMessage(Notifications.themeLight + "friends:");
        player.sendMessage(getFriendsAsString(plot));
    }


    private String getFriendsAsString(Plot plot){
        String friends = "";
        for(Friend friend: plot.getFriends()){
            friends += (", " + friend.getName());
        }
        if(friends.length() > 2){
            return friends.substring(2);
        }else{
            return "-";
        }
    }

}
