package com.eclipsekingdom.playerplot.plot.actions;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.validation.NameStatus;
import com.eclipsekingdom.playerplot.plot.validation.NameValidation;
import com.eclipsekingdom.playerplot.plot.validation.RegionStatus;
import com.eclipsekingdom.playerplot.plot.validation.RegionValidation;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Claim implements PlotAction {

    private PlayerPlot plugin;
    private PlotCache plotCache;
    private UserCache userCache;
    private PluginConfig pluginConfig;
    private RegionValidation regionValidation;

    public Claim(PlayerPlot plugin){
        this.plugin = plugin;
        this.plotCache = plugin.getPlotCache();
        this.userCache = plugin.getUserCache();
        this.pluginConfig = plugin.getPluginConfig();
        this.regionValidation = new RegionValidation(plugin);
    }

    @Override
    public void run(Player player, String[] args) {
        UserData userData = userCache.getData(player.getUniqueId());
        int usedPlots = userData.getPlotsUsed();
        int availablePlots = pluginConfig.getStartingPlotNum() + userData.getBonusPlots();
        int unitSideLength = pluginConfig.getPlotUnitSideLength();
        if(usedPlots < availablePlots){
            RegionStatus regionStatus = regionValidation.canPlotBeRegisteredAt(player.getLocation(), unitSideLength);
            if(regionStatus == RegionStatus.VALID){

                Plot plot;
                if(args.length > 1){
                    String plotName = args[1];
                    NameStatus nameStatus = NameValidation.clean(plotName, userData);
                    if(nameStatus == NameStatus.VALID){
                        plot = new Plot(player, player.getLocation(), plotName, unitSideLength);
                    }else{
                        Notifications.sendWarning(player, nameStatus.getMessage());
                        return;
                    }
                }else{
                    plot = new Plot(player, player.getLocation(), getDefaultName(userData), unitSideLength);
                }

                plotCache.addPlot(plot);
                userData.addPlot(plot);
                player.sendMessage(SUCCESSFUL_CLAIM_MESSAGE(plot.getName()));
                Scan.showPlot(player, plot, plugin, 7);
                player.playSound(player.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 1f, 1f);
            }else{
                Notifications.sendWarning(player, regionStatus.getMessage());
            }
        }else{
            Notifications.sendWarning(player, "Plot limit reached");
            Notifications.sendTip(player, "plot free", "to remove a plot");
            Notifications.sendTip(player,"plot list", "to view all your plots");
        }

    }

    private static final String SUCCESSFUL_CLAIM_MESSAGE(String plotName){
        return (Notifications.themeLight + "Plot "+ Notifications.themeDark + plotName+ Notifications.themeLight + " was claimed");
    }

    private String getDefaultName(UserData userData){
        int postFix = 0;
        List<Integer> namePostFixes = new ArrayList<>();
        for(Plot plot: userData.getPlots()){
            try{
                int onePostFix = Integer.parseInt(plot.getName().split("lot_")[1]);
                namePostFixes.add(onePostFix);
            }catch (Exception e){
                //do nothing
            }
        }
        for(int num: namePostFixes){
            if(postFix < num){
                postFix = num;
            }
        }
        return ("plot_" + (postFix+1));
    }
}
