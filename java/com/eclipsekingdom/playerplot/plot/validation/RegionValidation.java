package com.eclipsekingdom.playerplot.plot.validation;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.PlotPoint;
import org.bukkit.World;
import org.bukkit.Location;

public class RegionValidation {

    private PluginConfig pluginConfig;
    private PlotCache plotCache;

    public RegionValidation(PlayerPlot plugin){
        this.pluginConfig = plugin.getPluginConfig();
        this.plotCache = plugin.getPlotCache();
    }


    public RegionStatus canPlotBeRegisteredAt(Location center, int sideLength){

        if(!pluginConfig.getValidWorlds().contains(center.getWorld().getName())){
            return RegionStatus.INVALID_WORLD;
        }else if(overlapsPlayerPlot(center, sideLength, 0)){
            return RegionStatus.REGION_OCUPIED;
        }else if(overlapsTownisPlot(center, sideLength)){
            return RegionStatus.REGION_OCUPIED;
        }else if(overlapsWGRegion(center, sideLength)){
            return RegionStatus.REGION_OCUPIED;
        }else{
            return RegionStatus.VALID;
        }

    }

    public RegionStatus canPlotBeUpgradedAt(World world, PlotPoint center, int sideLength){
        Location centerLoc = center.asLocation(world);
        if(overlapsPlayerPlot(center.asLocation(world), sideLength, 1)){
            return RegionStatus.REGION_OCUPIED;
        }else if(overlapsTownisPlot(centerLoc, sideLength)){
            return RegionStatus.REGION_OCUPIED;
        }else if(overlapsWGRegion(centerLoc, sideLength)){
            return RegionStatus.REGION_OCUPIED;
        }else{
            return RegionStatus.VALID;
        }
    }



    //check overlaps with all plots in same zone
    public boolean overlapsPlayerPlot(Location center, int sideLength, int tolerance){
        int overlapCount = 0;
        for(Plot plot: plotCache.getPlotsNear(center, sideLength)){
            boolean overlaps;

            if(plot.calculateSideLength() >= sideLength){
                overlaps = overlaps(plot.getCorners(), PlotPoint.getCornersFromLocation(PlotPoint.fromLocation(center), sideLength));
            }else{
                overlaps = overlaps(PlotPoint.getCornersFromLocation(PlotPoint.fromLocation(center), sideLength), plot.getCorners());
            }

            if(overlaps){
                overlapCount ++;
            }
            if(overlapCount > tolerance){
                break;
            }
        }

        return (overlapCount > tolerance);
    }

    //check townis
    private static boolean overlapsTownisPlot(Location center, int sideLength){
        boolean overlaps = false;

        //if townis is pluggined in

        return overlaps;
    }

    //check worldguard
    private static boolean overlapsWGRegion(Location center, int sideLength){
        boolean overlaps = false;

        //if worldgaurd is pluggined in

        return overlaps;
    }


    private static boolean overlaps(PlotPoint[] biggerPlot, PlotPoint[] smallerPlot){
        boolean overlaps = false;
        for(PlotPoint plotCorner: smallerPlot){
            if(regionContainsPoint(biggerPlot, plotCorner)){
                overlaps = true;
                break;
            }
        }
        return overlaps;
    }

    private static boolean regionContainsPoint(PlotPoint[] container, PlotPoint plotCorner){
        PlotPoint min = container[0];
        PlotPoint max = container[1];
        boolean withinXRange = (min.getX() <= plotCorner.getX() && plotCorner.getX() <= max.getX());
        boolean withinZRange = (min.getZ() <= plotCorner.getZ() && plotCorner.getZ() <= max.getZ());
        return (withinXRange && withinZRange);
    }



}
