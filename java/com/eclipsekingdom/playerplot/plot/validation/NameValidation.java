package com.eclipsekingdom.playerplot.plot.validation;

import com.eclipsekingdom.playerplot.data.UserData;
import com.eclipsekingdom.playerplot.plot.Plot;

import java.util.List;
import java.util.UUID;

public class NameValidation {

    public static NameStatus clean(String name, UserData userData){
        if (!name.matches("^[a-zA-Z0-9\\_\\-]+$")) {
            return NameStatus.SPECIAL_CHARACTERS;
        }else if(name.length() > 20){
            return NameStatus.TOO_LONG;
        }else{
            boolean foundMatch = false;
            for(Plot plot: userData.getPlots()){
                if(plot.getName().equalsIgnoreCase(name)){
                    foundMatch = true;
                    break;
                }
            }
            if(foundMatch){
                return NameStatus.NAME_TAKEN;
            }else{
                return NameStatus.VALID;
            }
        }
    }

}
