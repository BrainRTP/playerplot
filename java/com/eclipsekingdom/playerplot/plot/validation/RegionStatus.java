package com.eclipsekingdom.playerplot.plot.validation;

public enum RegionStatus {

    VALID("success"),
    INVALID_WORLD("You cannot claim a plot in this world"),
    REGION_OCUPIED("Another region is too close. Your plot would overlap");

    RegionStatus(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    private final String message;

}
