package com.eclipsekingdom.playerplot.plot;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.util.*;

public class Plot {

    private UUID ID;
    private final UUID ownerID;
    private final String ownerName;
    private String name;
    private World world;
    private List<Friend> friends = new ArrayList<>();
    private PlotPoint minCorner;
    private PlotPoint maxCorner;
    private int relativeSize = 1;

    /* --- constructors ---*/

    public Plot(Player player, Location location, String name, int sideLength){
        this.ID = UUID.randomUUID();
        this.ownerID = player.getUniqueId();
        this.ownerName = player.getName();
        this.name = name;
        this.world = location.getWorld();
        this.minCorner = PlotPoint.getMinCornerFromCenter(PlotPoint.fromLocation(location), sideLength);
        this.maxCorner = PlotPoint.getMaxCornerFromCenter(PlotPoint.fromLocation(location), sideLength);
    }

    public Plot(UUID plotID,String name, UUID ownerID, String ownerName, PlotPoint minCorner, PlotPoint maxCorner, World world, int relativeSize, List<Friend> friends){
        this.ID = plotID;
        this.name = name;
        this.ownerID = ownerID;
        this.ownerName = ownerName;
        this.minCorner = minCorner;
        this.maxCorner = maxCorner;
        this.relativeSize = relativeSize;
        this.world = world;
        this.friends = friends;
    }


    /* --- interface ---*/

    public UUID getID() {return ID;}

    public UUID getOwnerID(){
        return ownerID;
    }

    public String getOwnerName(){
        return ownerName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public World getWorld(){
        return world;
    }

    public int getRelativeSize(){
        return relativeSize;
    }

    public void incrementRelativeSize(){
        relativeSize++;
    }

    public void decrementRelativeSize(){
        relativeSize--;
    }

    public boolean contains(Location location){
        return (location.getWorld().equals(world) && withinXRange(location) && withinZRange(location));
    }

    private boolean withinXRange(Location location){
        return ( (minCorner.getX() <= location.getBlockX()) && (location.getBlockX() <= maxCorner.getX()) );
    }

    private boolean withinZRange(Location location){
        return ( (minCorner.getZ() <= location.getBlockZ()) && (location.getBlockZ() <= maxCorner.getZ()) );
    }

    public PlotPoint[] getCorners(){
        PlotPoint[] corners = new PlotPoint[4];
        corners[0] = minCorner; //bottom left
        corners[1] = maxCorner; //top right
        corners[2] = new PlotPoint(minCorner.getX(), maxCorner.getZ());//top left
        corners[3] = new PlotPoint(maxCorner.getX(), minCorner.getZ());//bottom right
        return corners;
    }

    public PlotPoint getMinCorner() {
        return minCorner;
    }

    public PlotPoint getMaxCorner(){
        return maxCorner;
    }

    public void setRegion(PlotPoint minCorner, PlotPoint maxCorner){
        this.minCorner = minCorner;
        this.maxCorner = maxCorner;
    }

    public int calculateSideLength(){
        return ( (maxCorner.getX() - minCorner.getX()) + 1 );
    }

    public PlotPoint calculatePlotCenter(){
        int length = calculateSideLength();
        int x;
        int z;
        if(length%2==0){
            x = maxCorner.getX() - (length/2);
            z = maxCorner.getZ() - (length/2);
        }else{
            x = maxCorner.getX() - ((length - 1) / 2);
            z = maxCorner.getZ() - ((length - 1) / 2);
        }
        return new PlotPoint(x, z);
    }

    public boolean trusts(Player player){
        return trusts(player.getUniqueId());
    }

    public boolean trusts(UUID friendID){
        boolean isFriend = false;
        for(Friend friend: friends){
            if(friend.getUuid().equals(friendID)){
                isFriend = true;
                break;
            }
        }
        return isFriend;
    }

    public boolean trusts(String friendName){
        boolean isFriend = false;
        for(Friend friend: friends){
            if(friend.getName().equalsIgnoreCase(friendName)){
                isFriend = true;
                break;
            }
        }
        return isFriend;
    }

    public void addFriend(Friend friend){
        friends.add(friend);
    }

    public void removeFriend(String friendName){
        List<Friend> toRemove = new ArrayList<>();
        for(Friend friend: friends){
            if(friend.getName().equalsIgnoreCase(friendName)){
                toRemove.add(friend);
            }
        }
        friends.removeAll(toRemove);
    }

    public List<Friend> getFriends(){
        return friends;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Plot){
            Plot plot = (Plot) o;
            return ( this.ID.equals(plot.getID()) );
        }else{
            return super.equals(o);
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(19, 37).
                append(ID).
                toHashCode();
    }

}
