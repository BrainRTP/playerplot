package com.eclipsekingdom.playerplot.util;

import com.eclipsekingdom.playerplot.Permissions;
import com.eclipsekingdom.playerplot.PlayerPlot;
import com.google.common.collect.ImmutableSet;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AutoCompleteListener implements Listener {

    public AutoCompleteListener(PlayerPlot plugin){
        plugin.getServer().getPluginManager().registerEvents(this,plugin);
    }

    @EventHandler
    public void onComplete(TabCompleteEvent e){
        if(e.getSender() instanceof Player){
            Player player = (Player) e.getSender();
            if(e.getBuffer().contains("/plot trust ")){
                e.setCompletions(getRefinedCompletions("/plot trust", e.getBuffer(),onlineCompletions(player)));
            }else if(e.getBuffer().contains("/plot untrust ")){
                e.setCompletions(getRefinedCompletions("/plot untrust", e.getBuffer(),onlineCompletions(player)));
            }else if(e.getBuffer().contains("/plot ")){
                List<String> completions = new ArrayList<>();
                completions.addAll(plotCompletions);
                if(Permissions.canChechActivity(player)){
                    completions.add("activity");
                }
                e.setCompletions(getRefinedCompletions("/plot", e.getBuffer(), completions));
            }
        }
    }

    private List<String> getRefinedCompletions(String root, String buffer, List<String> completions){
        if(buffer.equalsIgnoreCase(root + " ")){
            return completions;
        }else{
            List<String> refinedCompletions = new ArrayList<>();
            String bufferFromRoot = buffer.split(root + " ")[1];
            for(String completion : completions){
                if(bufferFromRoot.length() < completion.length()){
                    if(completion.substring(0,bufferFromRoot.length()).equalsIgnoreCase(bufferFromRoot)){
                        refinedCompletions.add(completion);
                    }
                }
            }
            return refinedCompletions;
        }
    }

    private static final Set<String> plotCompletions = ImmutableSet.<String>builder()
            .add("help")
            .add("scan")
            .add("claim")
            .add("rename")
            .add("free")
            .add("info")
            .add("list")
            .add("trust")
            .add("untrust")
            .add("upgrade")
            .add("downgrade")
            .build();

    private static List<String> onlineCompletions(Player player){
        List<String> onlinePlayerName = new ArrayList<>();
        for(Player oPlayer: Bukkit.getOnlinePlayers()){
            if(oPlayer != player){
                onlinePlayerName.add(oPlayer.getName());
            }
        }
        return onlinePlayerName;
    }
}
