package com.eclipsekingdom.playerplot.util;

import com.eclipsekingdom.playerplot.Permissions;
import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachmentInfo;

import java.util.LinkedHashMap;
import java.util.Map;

public class PluginHelp {

    private UserCache userCache;
    private PluginConfig pluginConfig;

    public PluginHelp(PlayerPlot plugin){
        this.userCache = plugin.getUserCache();
        this.pluginConfig = plugin.getPluginConfig();
    }

    public void showTo(Player player){
        UserData userData = userCache.getData(player.getUniqueId());
        int usedPlots = userData.getPlotsUsed();
        int availablePlots = pluginConfig.getStartingPlotNum() + userData.getBonusPlots();
        player.sendMessage(ChatColor.LIGHT_PURPLE +""+ ChatColor.BOLD + "PlayerPlot" + ChatColor.ITALIC+""+ChatColor.DARK_PURPLE + " - your plots: (" + usedPlots + "/" + availablePlots + ")");
        player.sendMessage(ChatColor.YELLOW + "-------" + ChatColor.GOLD + " Commands " + ChatColor.YELLOW + "-------");

        for(Map.Entry<String, String> cmdDescriptString: commandToDescription.entrySet()){
            String cmdComponent = ChatColor.GOLD + "/" + cmdDescriptString.getKey();
            cmdComponent = cmdComponent.replace("[", ChatColor.RED +"[");
            cmdComponent = cmdComponent.replace("]", "]" + ChatColor.GOLD);

            String descriptComponent = ChatColor.RESET + cmdDescriptString.getValue();
            descriptComponent= descriptComponent.replace("[", ChatColor.RED + "[");
            descriptComponent = descriptComponent.replace("]", "]" + ChatColor.RESET);

            player.sendMessage(cmdComponent + ": " + descriptComponent);
        }

        if(Permissions.hasExtraCommands(player)){
            player.sendMessage(ChatColor.YELLOW + "-------" + ChatColor.GOLD + " Extra Commands " + ChatColor.YELLOW + "-------");
            if(Permissions.canViewAllPlots(player)){
                player.sendMessage(ChatColor.GOLD + "/allplots:" +ChatColor.RESET +" show all plots");
            }

            if(Permissions.canChechActivity(player)){
                player.sendMessage(ChatColor.GOLD + "/plot activity:" +ChatColor.RESET +" check the activity of the plot you are standing in");

            }

            if(Permissions.canDeletePlots(player)){
                player.sendMessage(ChatColor.GOLD + "/delplot:" +ChatColor.RESET +" delete the plot you are standing in");

            }

            if(Permissions.canSummonPlotDeed(player)){
                player.sendMessage(ChatColor.GOLD + "/plotdeed " + ChatColor.RED + "[count]" + ChatColor.GOLD + ":" +ChatColor.RESET +" summon plot deed(s)");

            }
        }


    }

    private LinkedHashMap<String,String> commandToDescription = buildCommandMap();

    private LinkedHashMap<String, String> buildCommandMap(){
        LinkedHashMap<String,String> commandMap = new LinkedHashMap<>();
        //commands
        commandMap.put("plot help", "show this message");
        commandMap.put("plot scan", "display plot boundary");
        commandMap.put("plot claim","claim a plot");
        commandMap.put("plot claim [name]","claim named plot");
        commandMap.put("plot rename [name]", "rename plot");
        commandMap.put("plot free","remove a plot");
        commandMap.put("plot info", "get plot details");
        commandMap.put("plot list", "list your plots");
        commandMap.put("plot trust [player]", "add [player] to plot");
        commandMap.put("plot untrust [player]", "remove [player] from plot");
        commandMap.put("plot upgrade", "increase plot size");
        commandMap.put("plot downgrade", "decrease plot size");
        return commandMap;
    }

}
