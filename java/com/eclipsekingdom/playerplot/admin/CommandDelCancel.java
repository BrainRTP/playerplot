package com.eclipsekingdom.playerplot.admin;

import com.eclipsekingdom.playerplot.PlayerPlot;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandDelCancel implements CommandExecutor {

    private ActiveDeleteRequests activeDeleteRequests;

    public CommandDelCancel(PlayerPlot plugin){
        this.activeDeleteRequests = plugin.getActiveDeleteRequests();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player){
            Player player = (Player) sender;
            if(activeDeleteRequests.hasActiveRequest(player)){
                activeDeleteRequests.deleteRequest(player);
                player.sendMessage(ChatColor.LIGHT_PURPLE + "Request canceled");
            }else{
                player.sendMessage(ChatColor.RED + "You do not have any pending plot deletion requests");
            }
        }

        return false;
    }
}
