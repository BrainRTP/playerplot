package com.eclipsekingdom.playerplot.admin;

import com.eclipsekingdom.playerplot.Permissions;
import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.util.InfoList;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CommandAllPlots implements CommandExecutor {

    private PlotCache plotCache;

    public CommandAllPlots(PlayerPlot plugin){
        this.plotCache = plugin.getPlotCache();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Permissions.canViewAllPlots(player)){

                List<String> items = new ArrayList<>();
                for(Plot plot: plotCache.getPlots()){
                    items.add(getListString(plot));
                }
                InfoList infoList = new InfoList(ChatColor.DARK_PURPLE + "All Plots",items, 7, "allplots");
                if(args.length == 0){
                    infoList.displayTo(player, 1);
                }else{
                    int pageNum = 1;
                    try{
                        pageNum = Integer.parseInt(args[0]);
                    }catch (Exception e){}

                    infoList.displayTo(player, pageNum);
                }
            }else{
                Notifications.sendWarning(player, "You do not have permission for this command.");
            }
        }

        return false;
    }

    private String getListString(Plot plot){
        String name = plot.getName();
        String ownerName = plot.getOwnerName();
        return (ChatColor.GRAY + name + " - " + ChatColor.LIGHT_PURPLE + ownerName);
    }

}
