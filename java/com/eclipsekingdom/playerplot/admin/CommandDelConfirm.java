package com.eclipsekingdom.playerplot.admin;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import com.eclipsekingdom.playerplot.plot.Plot;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class CommandDelConfirm implements CommandExecutor {

    private ActiveDeleteRequests activeDeleteRequests;
    private PlotCache plotCache;
    private UserCache userCache;

    public CommandDelConfirm(PlayerPlot plugin){
        this.activeDeleteRequests = plugin.getActiveDeleteRequests();
        this.plotCache = plugin.getPlotCache();
        this.userCache = plugin.getUserCache();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player){
            Player player = (Player) sender;
            if(activeDeleteRequests.hasActiveRequest(player)){
                Plot plot = activeDeleteRequests.getRequestedPlot(player);
                activeDeleteRequests.deleteRequest(player);
                UUID ownerID = plot.getOwnerID();
                if(Bukkit.getPlayer(ownerID) != null){
                    UserData userData = userCache.getData(ownerID);
                    userData.removePlot(plot);
                }else{
                    userCache.cacheSync(ownerID);
                    UserData userData = userCache.getData(ownerID);
                    userData.removePlot(plot);
                    userCache.forget(ownerID);
                }
                plotCache.removePlot(plot);
                player.sendMessage(ChatColor.LIGHT_PURPLE + "Plot deleted");
            }else{
                player.sendMessage(ChatColor.RED + "You do not have any pending plot deletion requests");
            }
        }

        return false;
    }
}
