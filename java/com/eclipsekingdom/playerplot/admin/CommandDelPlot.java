package com.eclipsekingdom.playerplot.admin;

import com.eclipsekingdom.playerplot.Permissions;
import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
public class CommandDelPlot implements CommandExecutor {

    public ActiveDeleteRequests activeDeleteRequests;
    public PlotCache plotCache;
    public PlayerPlot plugin;

    public CommandDelPlot(PlayerPlot plugin){
        this.activeDeleteRequests = plugin.getActiveDeleteRequests();
        this.plotCache = plugin.getPlotCache();
        this.plugin  = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Permissions.canDeletePlots(player)){
                Plot plot = plotCache.getPlot(player.getLocation());
                if(plot != null){
                    activeDeleteRequests.registerRequest(player, plot);
                    player.sendMessage(ChatColor.RED + "Are you sure you want to delete " + ChatColor.DARK_PURPLE + plot.getOwnerName() + ChatColor.RED + "'s plot?");
                    player.sendMessage(ChatColor.LIGHT_PURPLE + "Use /delconfirm to confirm or /delcancel to cancel.");
                    player.sendMessage(ChatColor.LIGHT_PURPLE + "The request is valid for 25 seconds");
                    Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        @Override
                        public void run() {
                            activeDeleteRequests.deleteRequest(player);
                        }
                    }, 20 * 25);
                }else{
                    Notifications.sendWarning(player, "You are not standing in a plot");
                }
            }else{
                Notifications.sendWarning(player, "You do not have permission for this command.");
            }
        }
        return false;
    }
}
