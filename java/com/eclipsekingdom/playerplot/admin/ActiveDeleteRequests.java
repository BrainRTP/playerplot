package com.eclipsekingdom.playerplot.admin;

import com.eclipsekingdom.playerplot.plot.Plot;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ActiveDeleteRequests {

    private Map<UUID, Plot> requesterToPlot = new HashMap<>();

    public void  registerRequest(Player player, Plot plot){
        requesterToPlot.put(player.getUniqueId(), plot);
    }

    public void deleteRequest(Player player){
        requesterToPlot.remove(player.getUniqueId());
    }

    public boolean hasActiveRequest(Player player){
        return requesterToPlot.containsKey(player.getUniqueId());
    }

    public Plot getRequestedPlot(Player player){
        return requesterToPlot.get(player.getUniqueId());
    }

}
