package com.eclipsekingdom.playerplot.admin;

import com.eclipsekingdom.playerplot.Permissions;
import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.data.PlotCache;
import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.actions.PlotAction;
import com.eclipsekingdom.playerplot.util.Notifications;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

public class ActionActivity implements PlotAction {

    private PlotCache plotCache;
    private UserCache userCache;

    public ActionActivity(PlayerPlot plugin){
        this.plotCache = plugin.getPlotCache();
        this.userCache = plugin.getUserCache();
    }

    @Override
    public void run(Player player, String[] args) {
        if(Permissions.canChechActivity(player)){
            Plot plot = plotCache.getPlot(player.getLocation());
            if(plot != null){
                UUID ownerID = plot.getOwnerID();
                if(Bukkit.getPlayer(ownerID) != null){
                    UserData userData = userCache.getData(ownerID);
                    Timestamp lastSeen = userData.getLastSeen();
                    sendActivity(player, plot, lastSeen);
                }else{
                    userCache.cacheSync(ownerID);
                    UserData userData = userCache.getData(ownerID);
                    Timestamp lastSeen = userData.getLastSeen();
                    sendActivity(player, plot, lastSeen);
                    userCache.forget(ownerID);
                }
            }else{
                player.sendMessage(Notifications.themeDark + "[PlayerPlot]: " + ChatColor.RED + "No plots detected");
            }
        }else{
            Notifications.sendWarning(player, "You do not have permission for this command.");
        }

    }

    private void sendActivity(Player player, Plot plot, Timestamp lastSeen){
        if(Bukkit.getPlayer(plot.getOwnerID()) != null){
            player.sendMessage(ChatColor.LIGHT_PURPLE + plot.getOwnerName() + " is online now.");
        }else{
            player.sendMessage(ChatColor.LIGHT_PURPLE + plot.getOwnerName() + " was last seen " + getTimeAgo(lastSeen));
        }
    }

    private String getTimeAgo(Timestamp timeStamp) {
        long now = Timestamp.from(Instant.now()).getTime()/1000;
        long lastSeen = timeStamp.getTime()/1000;
        int diff = (int)(now-lastSeen);
        int sec = diff;
        int min = Math.round(diff / 60 );
        int hrs = Math.round(diff / 3600);
        int days = Math.round(diff / 86400 );
        int weeks = Math.round(diff / 604800);
        int months = Math.round(diff / 2600640 );
        int years = Math.round(diff / 31207680 );

        if(sec <= 60) {
            return  sec + " seconds ago";
        }else if(min <= 60) {
            if(min==1) {
                return "one minute ago";
            }
            else {
                return min +" minutes ago";
            }
        }else if(hrs <= 24) {
            if(hrs == 1) {
                return "an hour ago";
            }
            else {
                return hrs + " hours ago";
            }
        }else if(days <= 7) {
            if(days == 1) {
                return "Yesterday";
            }
            else {
                return days + " days ago";
            }
        }else if(weeks <= 4.3) {
            if(weeks == 1) {
                return "a week ago";
            }
            else {
                return weeks + " weeks ago";
            }
        }else if(months <= 12) {
            if(months == 1) {
                return "a month ago";
            }
            else {
                return months + " months ago";
            }
        }else {
            if(years == 1) {
                return "one year ago";
            }
            else {
                return years + " years ago";
            }
        }
    }

}
