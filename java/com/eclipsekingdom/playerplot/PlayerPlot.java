package com.eclipsekingdom.playerplot;

import com.eclipsekingdom.playerplot.admin.*;
import com.eclipsekingdom.playerplot.data.*;
import com.eclipsekingdom.playerplot.plot.CommandPlayerPlot;
import com.eclipsekingdom.playerplot.plot.CommandPlots;
import com.eclipsekingdom.playerplot.plot.PlotProtection;
import com.eclipsekingdom.playerplot.plotdeed.CommandPlotDeed;
import com.eclipsekingdom.playerplot.plotdeed.PlotDeedListener;
import com.eclipsekingdom.playerplot.raid.CommandRaid;
import com.eclipsekingdom.playerplot.util.AutoCompleteListener;
import org.bukkit.plugin.java.JavaPlugin;

public final class PlayerPlot extends JavaPlugin {


    private PluginConfig pluginConfig;
    private PlotFlatFile plotFlatFile;
    private PlotCache plotCache;
    private UserFlatFile userFlatFile;
    private UserCache userCache;
    private Database database;
    private ActiveDeleteRequests activeDeleteRequests;
    private PlayTimeTracker playTimeTracker;

    public static PlayerPlot plugin;
    @Override
    public void onEnable() {
        this.plugin = this;
        pluginConfig = new PluginConfig();
        pluginConfig.load();

        database = new Database(this);
        if(pluginConfig.isUsingDatabase()){
            database.initialize();
        }

        plotFlatFile = new PlotFlatFile(this);
        plotCache = new PlotCache(this);
        plotCache.load();

        userFlatFile = new UserFlatFile(this);
        userCache = new UserCache(this);
        userCache.load();

        activeDeleteRequests = new ActiveDeleteRequests();

        playTimeTracker = new PlayTimeTracker(this);
        playTimeTracker.load();

        new PlotProtection(this);
        new PlotDeedListener(this);
        new AutoCompleteListener(this);

        this.getCommand("plot").setExecutor(new CommandPlayerPlot(this));
        this.getCommand("plots").setExecutor(new CommandPlots(this));
        this.getCommand("plotdeed").setExecutor(new CommandPlotDeed());
        this.getCommand("raid").setExecutor(new CommandRaid());

        this.getCommand("delplot").setExecutor(new CommandDelPlot(this));
        this.getCommand("delconfirm").setExecutor(new CommandDelConfirm(this));
        this.getCommand("delcancel").setExecutor(new CommandDelCancel(this));

        this.getCommand("allplots").setExecutor(new CommandAllPlots(this));

    }

    @Override
    public void onDisable() {
        playTimeTracker.save();
        userCache.save();
        plotCache.save();
    }

    public PluginConfig getPluginConfig(){
        return pluginConfig;
    }

    public UserCache getUserCache(){
        return userCache;
    }

    public UserFlatFile getUserFlatFile(){
        return userFlatFile;
    }

    public PlotCache getPlotCache(){
        return plotCache;
    }

    public PlotFlatFile getPlotFlatFile(){
        return plotFlatFile;
    }

    public Database getDatabase() {
        return database;
    }

    public ActiveDeleteRequests getActiveDeleteRequests(){
        return activeDeleteRequests;
    }
}

