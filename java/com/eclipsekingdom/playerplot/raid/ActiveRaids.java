package com.eclipsekingdom.playerplot.raid;

import com.eclipsekingdom.playerplot.plot.Plot;

import java.util.HashSet;
import java.util.Set;

public class ActiveRaids {

    private Set<Plot> plots = new HashSet<>();

    private ActiveRaids(){}
    public static final ActiveRaids getInstance(){ return ACTIVE_RAIDS;}
    private static final ActiveRaids ACTIVE_RAIDS = new ActiveRaids();


    public void startRaid(Raid raid){
        plots.add(raid.getPlot());
    }

    public void endRaid(Raid raid){
        plots.remove(raid.getPlot());
    }

    public boolean isBeingRaided(Plot plot){
        return  (plots.contains(plot));
    }
}
