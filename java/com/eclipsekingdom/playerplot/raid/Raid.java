package com.eclipsekingdom.playerplot.raid;

import com.eclipsekingdom.playerplot.plot.Plot;
import jdk.nashorn.internal.ir.Block;

import javax.xml.stream.Location;
import java.util.HashSet;
import java.util.Set;

public class Raid {

    private Plot plot;
    private Set<Block> brokenBlocks = new HashSet<>();
    private Set<Location> placedBlocks = new HashSet<>();

    public Raid(Plot plot){
        this.plot = plot;
    }

    public Plot getPlot(){
        return plot;
    }

    public void addBrokenBlock(Block block){
        brokenBlocks.add(block);
    }

    public void addPlacedBlock(Location location){
        placedBlocks.add(location);
    }

    public void removePlacedBlock(Location location){
        placedBlocks.remove(location);
    }

    public void doCleanUp(){

    }

}
