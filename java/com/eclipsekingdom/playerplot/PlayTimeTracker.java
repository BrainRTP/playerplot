package com.eclipsekingdom.playerplot;

import com.eclipsekingdom.playerplot.data.UserCache;
import com.eclipsekingdom.playerplot.data.UserData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayTimeTracker implements Listener {

    private Map<UUID, Integer> playerToStartTime = new HashMap<>();
    private UserCache userCache;
    private PlayerPlot plugin;
    private PluginConfig pluginConfig;

    public PlayTimeTracker(PlayerPlot plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.userCache = plugin.getUserCache();
        this.plugin = plugin;
        this.pluginConfig = plugin.getPluginConfig();
        if(pluginConfig.isUsePlaytime()){
            TimeLoop timeLoop = new TimeLoop();
            timeLoop.runTaskTimer(plugin, 60*20*1, 60*20*3);
        }
    }

    @EventHandler (priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onJoin(PlayerJoinEvent e){
        if(pluginConfig.isUsePlaytime()){
            setStartTime(e.getPlayer());
            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                @Override
                public void run() {
                    UserData userData = userCache.getData(e.getPlayer().getUniqueId());
                    if(userData != null){
                        if(userData.getPlayTimeProgress() >= userData.getPlayTimeThreshold()){
                            giftPlot(e.getPlayer(), userData);
                        }
                    }
                }
            }, 20 * 3);
        }
    }

    @EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onQuit(PlayerQuitEvent e){
        if(pluginConfig.isUsePlaytime()){
            updatePlayTime(e.getPlayer());
        }
    }

    public void setStartTime(Player player){
        playerToStartTime.put(player.getUniqueId(), (int) (System.currentTimeMillis() / 1000));
    }

    public void updatePlayTime(Player player){
        UserData userData = userCache.getData(player.getUniqueId());
        int timeToAdd = (int)(System.currentTimeMillis()/1000) - playerToStartTime.get(player.getUniqueId());
        userData.incrementTimeProgress(timeToAdd);
        setStartTime(player);
    }

    public void load(){
        for(Player player: Bukkit.getOnlinePlayers()){
            setStartTime(player);
        }
    }

    public void save(){
        for(Player player: Bukkit.getOnlinePlayers()){
            updatePlayTime(player);
        }
    }

    private void giftPlot(Player player, UserData userData){
        userData.resetPlayTimeProgress();
        userData.updateThreshold();
        userData.grantBonusPlot();
        player.playSound(player.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE,0.5f,1.8f);
        player.sendTitle(ChatColor.DARK_PURPLE + "+1 Plot", "", 1*20, 2*20, 1*20);
    }

    private class TimeLoop extends BukkitRunnable {
        @Override
        public void run() {
            for(Player player : Bukkit.getOnlinePlayers()){
                UserData userData = userCache.getData(player.getUniqueId());
                if(userData!= null){
                    updatePlayTime(player);
                    if(userData.getPlayTimeProgress() >= userData.getPlayTimeThreshold()){
                        giftPlot(player, userData);
                    }
                }
            }

        }
    }



}
