package com.eclipsekingdom.playerplot;

import com.google.common.collect.ImmutableSet;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PluginConfig {

    private String PLUGIN_CONFIG_HEADER = "Plot Options";

    private File configFile = new File("plugins/PlayerPlot", "config.yml");
    private FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

    private String STARTING_PLOTS_SETTING = "Starting Plot Amount";
    private int STARTING_PLOTS_DEFAULT = 1;
    private int startingPlotNum;

    private String MAX_PLOTS_SETTING = "Maximum Plot Amount";
    private int MAX_PLOTS_DEFAULT = 50;
    private int maxPlotNum;

    private String USE_PLAYTIME_STRING = "Plots for playtime";
    private boolean USE_PLAYTIME_DEFAULT = true;
    private boolean usePlaytime;

    private String THRESHOLD_STRING = "Playtime threshold (seconds)";
    private int THRESHOLD_DEFAULT = 36000;
    private int threshold;

    private String SEQUENTIAL_THRESHOLD_INCREMENT_STRING = "Sequential threshold increment (seconds)";
    private int SEQUENTIAL_THRESHOLD_INCREMENT_DEFAULT = 3600;
    private int sequentialThresholdIncrement;


    private String maxThresholdSetting = "Maximum threshold (seconds)";
    private int maxThresholdDefault = 86400;
    private int maxThreshold;

    private String UNIT_SIZE_SETTING = "Plot Unit Size";
    private int UNIT_SIZE_DEFAULT = 25;
    private int unitSize;
    private String VALID_WORLDS_SETTING = "Valid Worlds";
    private Set<String> VALID_WORLDS_DEFAULT = ImmutableSet.<String>builder()
            .add("world")
            .add("world_nether")
            .add("world_the_end")
            .build();
    private Set<String> validWorlds;

    private String useDatabaseSetting = "Use database";
    private boolean useDatabaseDefault = false;
    private boolean useDatabase;

    private String hostSetting = "host";
    private String hostDefault = "00.00.000.00";
    private String host;

    private String portSetting = "port";
    private String portDefault = "3306";
    private String port;

    private String databaseSetting = "database";
    private String databaseDefault = "myDatabase";
    private String database;

    private String userSetting = "username";
    private String userDefault = "myUsername";
    private String user;

    private String passSetting = "password";
    private String passDefault = "myPassword";
    private String pass;

    public void load(){
        if(config.contains(PLUGIN_CONFIG_HEADER)){
            try{
                String key = PLUGIN_CONFIG_HEADER;
                startingPlotNum = config.getInt(key+"."+STARTING_PLOTS_SETTING);
                maxPlotNum = config.getInt(key+"."+MAX_PLOTS_SETTING);
                unitSize = config.getInt(key+"."+UNIT_SIZE_SETTING);
                validWorlds = new HashSet<>();
                validWorlds.addAll(config.getStringList(key+"."+VALID_WORLDS_SETTING));
                loadWorlds(validWorlds);
                useDatabase = config.getBoolean(key+"."+useDatabaseSetting);
                host = config.getString(key+"."+hostSetting);
                port = config.getString(key+"."+portSetting);
                database = config.getString(key+"."+databaseSetting);
                user = config.getString(key+"."+userSetting);
                pass = config.getString(key+"."+passSetting);
                usePlaytime = config.getBoolean(key + "."+USE_PLAYTIME_STRING);
                threshold = config.getInt(key + "."+THRESHOLD_STRING);
                sequentialThresholdIncrement = config.getInt(key + "."+SEQUENTIAL_THRESHOLD_INCREMENT_STRING);
                maxThreshold = config.getInt(key + "." + maxThresholdSetting);
            }catch (Exception e){
                loadDefaults();
                createDefault();
            }
        }else{
            loadDefaults();
            createDefault();
        }
    }

    private void createDefault(){
        String key = PLUGIN_CONFIG_HEADER;
        config.set(key+"."+STARTING_PLOTS_SETTING, STARTING_PLOTS_DEFAULT);
        config.set(key+"."+MAX_PLOTS_SETTING, MAX_PLOTS_DEFAULT);
        config.set(key+"."+UNIT_SIZE_SETTING, UNIT_SIZE_DEFAULT);
        ArrayList<String> validWorldList = new ArrayList<>();
        validWorldList.addAll(VALID_WORLDS_DEFAULT);
        config.set(key+"."+VALID_WORLDS_SETTING, validWorldList);
        config.set(key+"."+useDatabaseSetting, useDatabaseDefault);
        config.set(key+"."+hostSetting, hostSetting);
        config.set(key+"."+portSetting, portDefault);
        config.set(key+"."+databaseSetting, databaseDefault);
        config.set(key+"."+userSetting, userDefault);
        config.set(key+"."+passSetting, passDefault);
        config.set(key + "."+USE_PLAYTIME_STRING, USE_PLAYTIME_DEFAULT);
        config.set(key + "."+THRESHOLD_STRING, THRESHOLD_DEFAULT);
        config.set(key + "."+SEQUENTIAL_THRESHOLD_INCREMENT_STRING, SEQUENTIAL_THRESHOLD_INCREMENT_DEFAULT);
        config.set(key + "."+maxThresholdSetting, maxThresholdDefault);
        save();
    }

    private void save(){
        try{
            config.save(configFile);
        } catch (Exception e){
            Bukkit.getConsoleSender().sendMessage("[PlayerPlot] Error saving config.yml");
        }
    }

    private void loadDefaults(){
        startingPlotNum = STARTING_PLOTS_DEFAULT;
        maxPlotNum = MAX_PLOTS_DEFAULT;
        unitSize = UNIT_SIZE_DEFAULT;
        validWorlds = VALID_WORLDS_DEFAULT;
        useDatabase = useDatabaseDefault;
        host = hostDefault;
        port = portDefault;
        database = databaseDefault;
        user = userDefault;
        pass = passDefault;
        usePlaytime = USE_PLAYTIME_DEFAULT;
        threshold = THRESHOLD_DEFAULT;
        sequentialThresholdIncrement = SEQUENTIAL_THRESHOLD_INCREMENT_DEFAULT;
        maxThreshold = maxThresholdDefault;
    }

    private void loadWorlds(Set<String> worldStrings){
        for(String worldString: worldStrings){
            if(Bukkit.getWorld(worldString)  == null){
                World world = Bukkit.getServer().createWorld(new WorldCreator(worldString));
                Bukkit.getServer().getWorlds().add(world);
            }
        }
    }

    public int getStartingPlotNum(){
        return startingPlotNum;
    }

    public int getMaxPlotNum(){
        return maxPlotNum;
    }

    public int getPlotUnitSideLength(){
        return unitSize;
    }

    public Set<String> getValidWorlds(){
        return validWorlds;
    }

    public boolean isUsingDatabase(){
        return useDatabase;
    }

    public String getHost(){
        return host;
    }

    public String getPort(){
        return port;
    }

    public String getDatabase(){
        return database;
    }

    public String getUsername(){
        return user;
    }

    public String getPassword(){
        return pass;
    }

    public boolean isUsePlaytime(){
        return usePlaytime;
    }

    public int getThreshold(){
        return threshold;
    }

    public int getSequentialThresholdIncrement(){
        return sequentialThresholdIncrement;
    }

    public int getMaxThresholdDefault(){
        return maxThreshold;
    }

}
