package com.eclipsekingdom.playerplot.data;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.plot.Friend;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.PlotPoint;
import com.eclipsekingdom.playerplot.util.ConsoleSender;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlotFlatFile {

    private PluginConfig pluginConfig;

    public PlotFlatFile(PlayerPlot plugin){
        this.pluginConfig = plugin.getPluginConfig();
    }

    private final File plotFile = new File("plugins/PlayerPlot", "plots.yml");
    private final FileConfiguration plotConfig = YamlConfiguration.loadConfiguration(plotFile);
    private final String PLOT_HEADER = "Plots";

    public void store(UUID plotID, Plot plot){
        String key = PLOT_HEADER + "." + plotID;
        if(plot != null){
            plotConfig.set(key + ".name", plot.getName());
            plotConfig.set(key + ".ownerID", plot.getOwnerID().toString());
            plotConfig.set(key + ".ownerName", plot.getOwnerName());
            PlotPoint minCorner = plot.getMinCorner();
            plotConfig.set(key + ".minCorner", minCorner.getX()+"_"+minCorner.getZ());
            PlotPoint maxCorner = plot.getMaxCorner();
            plotConfig.set(key + ".maxCorner", maxCorner.getX()+"_"+maxCorner.getZ());
            plotConfig.set(key + ".world", plot.getWorld().getName());
            plotConfig.set(key + ".relativeSize", plot.getRelativeSize());
            String friendKey = key + ".friends";
            for(Friend friend: plot.getFriends()){
                plotConfig.set(friendKey + "." + friend.getUuid(), friend.getName());
            }
        }else{
            plotConfig.set(key,null);
        }
    }

    public List<Plot> fetch(){
        List<Plot> plots = new ArrayList<>();
        if(plotConfig.contains(PLOT_HEADER)){
            for(String plotID: plotConfig.getConfigurationSection(PLOT_HEADER).getKeys(false)){
                try{
                    String key = PLOT_HEADER + "." + plotID;
                    String plotName = plotConfig.getString(key + ".name");
                    UUID ownerID = UUID.fromString(plotConfig.getString(key + ".ownerID"));
                    String ownerName = plotConfig.getString(key + ".ownerName");
                    String minCornerString = plotConfig.getString(key + ".minCorner");
                    String[] minCornerStringParts = minCornerString.split("_");
                    int minX = Integer.valueOf(minCornerStringParts[0]);
                    int minZ = Integer.valueOf(minCornerStringParts[1]);
                    PlotPoint min = new PlotPoint(minX, minZ);
                    String maxCornerString = plotConfig.getString(key + ".maxCorner");
                    String[] maxCornerStringParts = maxCornerString.split("_");
                    int maxX = Integer.valueOf(maxCornerStringParts[0]);
                    int maxZ = Integer.valueOf(maxCornerStringParts[1]);
                    PlotPoint max = new PlotPoint(maxX, maxZ);
                    String worldName = plotConfig.getString(key + ".world");
                    int relativeSize = plotConfig.getInt(key + ".relativeSize");
                    List<Friend> friends = buildFriendList(key + ".friends");

                    if(pluginConfig.getValidWorlds().contains(worldName)){
                        plots.add(new Plot(UUID.fromString(plotID), plotName, ownerID, ownerName, min, max, Bukkit.getWorld(worldName), relativeSize, friends));
                    }
                }catch (Exception e){
                    ConsoleSender.sendMessage("Error loading plot: " + plotID);
                }
            }
        }
        return plots;
    }

    private List<Friend> buildFriendList(String friendKey){
        List<Friend> friends = new ArrayList<>();
        if(plotConfig.contains(friendKey)){
            for(String uuidString: plotConfig.getConfigurationSection(friendKey).getKeys(false)){
                UUID friendID = UUID.fromString(uuidString);
                String friendName = plotConfig.getString(friendKey + "." + uuidString);
                Friend friend = new Friend(friendID, friendName);
                friends.add(friend);
            }
        }
        return friends;
    }

    public void save(){
        try{
        plotConfig.save(plotFile);
        } catch (Exception e){
            Bukkit.getConsoleSender().sendMessage("Error saving plots.yml");
        }
    }




}
