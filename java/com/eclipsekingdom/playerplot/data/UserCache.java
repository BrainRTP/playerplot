package com.eclipsekingdom.playerplot.data;

import com.eclipsekingdom.playerplot.PlayerPlot;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

public class UserCache implements Listener {

    private UserFlatFile userFlatFile;
    private Map<UUID,UserData> userToData = new HashMap<>();
    private boolean usingDatabase;
    private Database database;

    public UserCache(PlayerPlot plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.userFlatFile = plugin.getUserFlatFile();
        this.usingDatabase = plugin.getPluginConfig().isUsingDatabase();
        this.database = plugin.getDatabase();
    }

    public void put(UUID playerID, UserData userData){
        userToData.put(playerID, userData);
    }

    public UserData getData(UUID playerID){
        return userToData.get(playerID);
    }

    @EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onJoin(PlayerJoinEvent e){
        cache(e.getPlayer().getUniqueId());
    }

    @EventHandler (priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onQuit(PlayerQuitEvent e){
        UUID playerID = e.getPlayer().getUniqueId();
        UserData userData = userToData.get(playerID);
        userData.setLastSeen(Timestamp.from(Instant.now()));
        forget(playerID);
    }

    public void load(){
        for(Player player: Bukkit.getOnlinePlayers()){
            cache(player.getUniqueId());
        }
    }

    public void save(){
        for(UUID playerID: userToData.keySet()){
            UserData userData = userToData.get(playerID);
            if(usingDatabase){
                database.storeUserSync(playerID, userData);
            }else{
                userFlatFile.store(playerID, userData);
            }
        }
    }

    private void cache(UUID playerID){
        if(usingDatabase){
            database.fetchUserDataAsync(playerID);
        }else{
            UserData userData = userFlatFile.fetch(playerID);
            userToData.put(playerID, userData);
        }
    }

    public void cacheSync(UUID playerID){
        if(usingDatabase){
            database.fetchUserDataSync(playerID);
        }else{
            UserData userData = userFlatFile.fetch(playerID);
            userToData.put(playerID, userData);
        }
    }

    public void forget(UUID playerID){
        UserData userData = userToData.get(playerID);
        if(usingDatabase){
            database.storeUserAsync(playerID, userData);
        }else{
            userFlatFile.store(playerID, userData);
        }
        userToData.remove(playerID);
    }

}
