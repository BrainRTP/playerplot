package com.eclipsekingdom.playerplot.data;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.plot.Friend;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.PlotPoint;
import org.bukkit.World;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Database {

    private String host;
    private int port;
    private String database;
    private String username;
    private String password;

    private PlayerPlot plugin;
    private Connection connection;

    public Database(PlayerPlot plugin){
        this.plugin = plugin;
        PluginConfig pluginConfig = plugin.getPluginConfig();
        this.host = pluginConfig.getHost();
        this.port = Integer.parseInt(pluginConfig.getPort());
        this.database = pluginConfig.getDatabase();
        this.username = pluginConfig.getUsername();
        this.password = pluginConfig.getPassword();
    }

    public void initialize(){
        try {
            openConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS PUser (" +
                    "uuid char(36)," +
                    "bonusPlots int(4) DEFAULT 0," +
                    "timeProgress int DEFAULT 0," +
                    "timeThreshold int DEFAULT 36000," +
                    "lastSeen timestamp DEFAULT CURRENT_TIMESTAMP," +
                    "PRIMARY KEY (uuid)" +
                    ");");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS PPlot (" +
                    "uuid char(36)," +
                    "name varchar(20)," +
                    "ownerID varchar(36)," +
                    "ownerName varchar(16)," +
                    "minX int," +
                    "minZ int," +
                    "maxX int," +
                    "maxZ int," +
                    "world varchar(36)," +
                    "relativeSize smallint," +
                    "PRIMARY KEY (uuid)" +
                    ");");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS PTrusts (" +
                    "plotID char(36)," +
                    "friendID varchar(36)," +
                    "friendName varchar(16)," +
                    "PRIMARY KEY (plotID, friendID) " +
                    ");");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS POwns (" +
                    "ownerID char(36)," +
                    "plotID char(36)," +
                    "PRIMARY KEY (ownerID,plotID)" +
                    ");");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void openConnection() throws SQLException, ClassNotFoundException {
        if (connection != null && !connection.isClosed()) {
            return;
        }
        synchronized (this) {
            if (connection != null && !connection.isClosed()) {
                return;
            }
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database, this.username, this.password);
        }
    }

    public void fetchUserDataSync(UUID playerID){
        fetchUserData(playerID);
    }

    public void fetchUserDataAsync(UUID playerID){
        BukkitRunnable r = new BukkitRunnable() {
            @Override
            public void run() {
                fetchUserData(playerID);
            }
        };

        r.runTaskAsynchronously(plugin);
    }

    public void fetchUserData(UUID playerID){
        try {
            openConnection();
            ResultSet userResult = connection.createStatement().executeQuery("SELECT * FROM PUser WHERE uuid = '"+playerID+"';");
            if(!userResult.next()){
                plugin.getUserCache().put(playerID, new UserData(plugin));
            }else{
                ResultSet plotResult = connection.createStatement().executeQuery("SELECT * FROM POwns WHERE ownerID = '"+playerID+"';");
                plugin.getUserCache().put(playerID, extractUserData(userResult, plotResult));
            }
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    private UserData extractUserData(ResultSet userResult, ResultSet plotResult) throws SQLException {
        int bonusPlots = userResult.getInt("bonusPlots");
        List<Plot> plots = new ArrayList<>();

        int timeProgress = userResult.getInt("timeProgress");
        int timeThreshold = userResult.getInt("timeThreshold");
        Timestamp lastSeen = userResult.getTimestamp("lastSeen");

        while(plotResult.next()){
            plots.add(plugin.getPlotCache().getPlot(UUID.fromString(plotResult.getString("plotID"))));
        }

        return  new UserData(bonusPlots, plots, timeProgress, timeThreshold, lastSeen, plugin);
    }

    public void storeUserAsync(UUID playerID, UserData userData){
        BukkitRunnable r = new BukkitRunnable() {
            @Override
            public void run() {
                storeUser(playerID, userData);
            }
        };
        r.runTaskAsynchronously(plugin);
    }

    public void storeUserSync(UUID playerID, UserData userData){
        storeUser(playerID, userData);
    }

    private void storeUser(UUID playerID, UserData userData){
        int bonusPlots = userData.getBonusPlots();
        int timeProgress=  userData.getPlayTimeProgress();
        int timeThreshold=  userData.getPlayTimeThreshold();
        Timestamp lastSeen = userData.getLastSeen();
        List<Plot> plots = userData.getPlots();
        try {
            openConnection();
            connection.createStatement().executeUpdate("REPLACE INTO PUser (uuid, bonusPlots, timeProgress, timeThreshold, lastSeen)" +
                    "VALUES ('"+playerID+"', "+bonusPlots+", "+timeProgress+", "+timeThreshold+", '"+lastSeen+"');");
            connection.createStatement().executeUpdate("DELETE FROM POwns WHERE ownerID = '"+playerID+"'");
            for(Plot plot: plots){
                connection.createStatement().executeUpdate("INSERT INTO POwns (ownerID, plotID) VALUES ('"+playerID+"', '"+plot.getID()+"');");
            }
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Plot> fetchPlots(){
        List<Plot> plots = new ArrayList<>();
        try {
            openConnection();
            ResultSet plotResults = connection.createStatement().executeQuery("SELECT * FROM PPlot;");
            while (plotResults.next()){
                Plot plot = extractPlot(plotResults);
                plots.add(plot);
            }
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return plots;
    }

    private Plot extractPlot(ResultSet plotResults) throws SQLException {
        UUID plotID = UUID.fromString(plotResults.getString("uuid"));
        String name = plotResults.getString("name");
        UUID ownerID = UUID.fromString(plotResults.getString("ownerID"));
        String ownerName = plotResults.getString("ownerName");
        int minX = plotResults.getInt("minX");
        int minZ = plotResults.getInt("minZ");
        PlotPoint min = new PlotPoint(minX, minZ);
        int maxX = plotResults.getInt("maxX");
        int maxZ = plotResults.getInt("maxZ");
        PlotPoint max = new PlotPoint(maxX, maxZ);
        World world = Bukkit.getWorld(plotResults.getString("world"));
        int relativeSize = plotResults.getInt("relativeSize");
        ResultSet friendsResult = connection.createStatement().executeQuery("SELECT * FROM PTrusts WHERE plotID = '"+plotID+"';");
        List<Friend> friends = new ArrayList<>();
        while(friendsResult.next()){
            UUID friendID = UUID.fromString(friendsResult.getString("friendID"));
            String friendName = friendsResult.getString("friendName");
            friends.add(new Friend(friendID, friendName));
        }
        return new Plot(plotID, name, ownerID, ownerName, min, max, world, relativeSize, friends);
    }


    public void storePlot(UUID plotID, Plot plot){
        try {
            openConnection();
            if(plot != null){
                String name = plot.getName();
                UUID ownerID = plot.getOwnerID();
                String ownerName = plot.getOwnerName();
                PlotPoint min = plot.getMinCorner();
                int minX = min.getX();
                int minZ = min.getZ();
                PlotPoint max = plot.getMaxCorner();
                int maxX = max.getX();
                int maxZ = max.getZ();
                String world = plot.getWorld().getName();
                int relativeSize = plot.getRelativeSize();
                List<Friend> friends = plot.getFriends();
                connection.createStatement().executeUpdate("REPLACE INTO PPlot (uuid, name, ownerID, ownerName, minX, minZ, maxX, maxZ, world, relativeSize) " +
                        "VALUES ('"+plotID+"', '"+name+"', '"+ownerID+"', '"+ownerName+"', "+minX+", "+minZ+", "+maxX+", "+maxZ+", '"+world+"', "+relativeSize+");");

                connection.createStatement().executeUpdate("DELETE FROM PTrusts WHERE plotID = '"+plotID+"'");
                for(Friend friend: friends){
                    connection.createStatement().executeUpdate("INSERT INTO PTrusts (plotID, friendID, friendName) " +
                            "VALUES ('"+plotID+"','"+friend.getUuid()+"', '"+friend.getName()+"');");
                }
            }else{
                connection.createStatement().executeUpdate("DELETE FROM PTrusts WHERE plotID = '"+plotID+"'");
                connection.createStatement().executeUpdate("DELETE FROM PPlot WHERE uuid = '"+plotID+"'");
            }
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        } catch(SQLException e) {
            e.printStackTrace();
        }

    }


}
