package com.eclipsekingdom.playerplot.data;

import com.eclipsekingdom.playerplot.plot.PlotPoint;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Location;

public class GridZone {

    private int x;
    private int z;

    public GridZone(int x, int z){
        this.x = x;
        this.z = z;
    }

    public static final GridZone fromLocation(Location location, int size){

        boolean leftOfXAxis = (location.getBlockX() < 0);
        int x = location.getBlockX()/size;
        if(leftOfXAxis){
            x --;
        }

        boolean belowZAxis = (location.getBlockZ() < 0);
        int z = location.getBlockZ()/size;
        if(belowZAxis){
            z --;
        }

        return new GridZone(x,z);
    }

    public static final GridZone fromPlotPoint(PlotPoint plotPoint, int size){

        boolean leftOfXAxis = (plotPoint.getX() < 0);
        int x = plotPoint.getX()/size;
        if(leftOfXAxis){
            x --;
        }

        boolean belowZAxis = (plotPoint.getZ() < 0);
        int z = plotPoint.getZ()/size;
        if(belowZAxis){
            z --;
        }

        return new GridZone(x,z);
    }

    public int getX(){
        return x;
    }

    public int getZ() {
        return z;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof GridZone){
            GridZone gridZone = (GridZone) o;
            return (this.x == gridZone.getX() && this.z == gridZone.getZ());
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(x).append(z).toHashCode();
    }


}
