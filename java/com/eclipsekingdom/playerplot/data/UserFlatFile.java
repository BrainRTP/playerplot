package com.eclipsekingdom.playerplot.data;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.util.ConsoleSender;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserFlatFile {

    private PlotCache plotCache;
    private PlayerPlot plugin;

    public UserFlatFile(PlayerPlot plugin){
        this.plotCache = plugin.getPlotCache();
        this.plugin = plugin;
    }

    public UserData fetch(UUID playerID){
        File userDataFile = new File("plugins/PlayerPlot/UserData", playerID + ".yml");
        if(userDataFile.exists()){
            try{
                FileConfiguration userDataConfig = YamlConfiguration.loadConfiguration(userDataFile);
                int bonusPlots = userDataConfig.getInt("bonusPlots");
                List<String> plotIDs = userDataConfig.getStringList("plots");
                List<Plot> plots = new ArrayList<>();
                for(String plotID: plotIDs){
                    Plot plot = plotCache.getPlot(UUID.fromString(plotID));
                    if(plot != null){
                        plots.add(plotCache.getPlot(UUID.fromString(plotID)));
                    }
                }
                int timeProgress = userDataConfig.getInt("timeProgress");
                int timeThreshold = userDataConfig.getInt("timeThreshold");
                Timestamp lastSeen = Timestamp.valueOf(userDataConfig.getString("lastSeen"));
                return new UserData(bonusPlots, plots, timeProgress, timeThreshold, lastSeen, plugin);
            }catch (Exception e){
                ConsoleSender.sendMessage("Error loading user: "+ playerID);
                return new UserData(plugin);
            }
        }else{
            return new UserData(plugin);
        }
    }

    public void store(UUID playerID, UserData userData){
        File userDataFile = new File("plugins/PlayerPlot/UserData", playerID + ".yml");
        FileConfiguration userDataConfig = YamlConfiguration.loadConfiguration(userDataFile);
        userDataConfig.set("bonusPlots", userData.getBonusPlots());
        List<String> plotIDs = new ArrayList<>();
        for(Plot plot: userData.getPlots()){
            if(plot != null){
                plotIDs.add(plot.getID().toString());
            }
        }
        userDataConfig.set("plots", plotIDs);
        userDataConfig.set("timeProgress", userData.getPlayTimeProgress());
        userDataConfig.set("timeThreshold", userData.getPlayTimeThreshold());
        userDataConfig.set("lastSeen", userData.getLastSeen().toString());
        save(userDataConfig, userDataFile);
    }

    private void save(FileConfiguration fileConfiguration, File file){
        try{
            fileConfiguration.save(file);
        } catch (Exception e){
            Bukkit.getConsoleSender().sendMessage("Error saving "+file.getName());
        }
    }

}
