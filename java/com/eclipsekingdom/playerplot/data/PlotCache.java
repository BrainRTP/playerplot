package com.eclipsekingdom.playerplot.data;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.plot.Plot;
import com.eclipsekingdom.playerplot.plot.PlotPoint;
import com.eclipsekingdom.playerplot.util.MapOperations;
import org.bukkit.Location;

import java.util.*;

public class PlotCache {

    private PlotFlatFile plotFlatFile;
    private HashMap<UUID, Plot> IDToPlot = new HashMap<>();
    private final HashMap<GridZone, List<Plot>> zoneToPlots = new HashMap<>();
    private final Set<UUID> unsavedPlots = new HashSet<>();
    private int gridGroupAmount;
    private boolean usingDatabase;
    private Database database;

    public PlotCache(PlayerPlot plugin){
        this.plotFlatFile = plugin.getPlotFlatFile();
        this.gridGroupAmount = plugin.getPluginConfig().getPlotUnitSideLength() * 16;
        this.usingDatabase = plugin.getPluginConfig().isUsingDatabase();
        this.database = plugin.getDatabase();
    }

    public void load(){
        List<Plot> plots = (usingDatabase)? database.fetchPlots() : plotFlatFile.fetch();
        for(Plot plot: plots){
            IDToPlot.put(plot.getID(), plot);
            assignToZones(plot);
        }
    }

    private void assignToZones(Plot plot){
        Set<GridZone> zones = new HashSet<>();
        for(PlotPoint corner: plot.getCorners()){
            zones.add( GridZone.fromPlotPoint(corner, gridGroupAmount) );
        }

        for(GridZone gridZone : zones){
            MapOperations.addItemToList(zoneToPlots, gridZone, plot);
        }
    }

    public void save(){
        if(usingDatabase){
            for(UUID plotID: unsavedPlots){
                database.storePlot(plotID,IDToPlot.get(plotID));
            }
        }else{
            for(UUID plotID: unsavedPlots){
                plotFlatFile.store(plotID,IDToPlot.get(plotID));
            }
        }

        plotFlatFile.save();
        unsavedPlots.clear();
    }

    public Plot getPlot(Location location){
        Plot requestedPlot = null;
        GridZone zone = GridZone.fromLocation(location, gridGroupAmount);
        if(zoneToPlots.containsKey(zone)){
            for(Plot plot: zoneToPlots.get(zone)){
                if(plot.contains(location)){
                    requestedPlot = plot;
                    break;
                }
            }
        }
        return requestedPlot;
    }

    public Plot getPlot(UUID plotID){
        return IDToPlot.get(plotID);
    }

    public Collection<Plot> getPlots(){
        return IDToPlot.values();
    }

    public Set<Plot> getPlotsNear(Location location, int sideLength){
        Set<Plot> closePlots = new HashSet<>();
        for(PlotPoint plotCorner: PlotPoint.getCornersFromLocation(PlotPoint.fromLocation(location), sideLength)){
            GridZone zone = GridZone.fromPlotPoint(plotCorner, gridGroupAmount);
            if(zoneToPlots.containsKey(zone)){
                for(Plot plot: zoneToPlots.get(zone)){
                    if(location.getWorld().equals(plot.getWorld())){
                        closePlots.add(plot);
                    }
                }
            }
        }
        return closePlots;
    }

    public void addPlot(Plot plot){
        IDToPlot.put(plot.getID(), plot);
        unsavedPlots.add(plot.getID());
        assignToZones(plot);
    }

    public void removePlot(Plot plot){
        IDToPlot.remove(plot.getID());
        unsavedPlots.add(plot.getID());
        unassignFromZones(plot);
    }

    private void unassignFromZones(Plot plot){
        Set<GridZone> zones = new HashSet<>();
        for(PlotPoint corner: plot.getCorners()){
            zones.add( GridZone.fromPlotPoint(corner,gridGroupAmount) );
        }

        for(GridZone gridZone : zones){
            MapOperations.removeItemFromList(zoneToPlots, gridZone, plot);
        }
    }

    public void reportPlotModification(Plot plot){
        unsavedPlots.add(plot.getID());
    }


}
