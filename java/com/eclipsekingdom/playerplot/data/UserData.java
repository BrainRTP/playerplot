package com.eclipsekingdom.playerplot.data;

import com.eclipsekingdom.playerplot.PlayerPlot;
import com.eclipsekingdom.playerplot.PluginConfig;
import com.eclipsekingdom.playerplot.plot.Plot;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class UserData {
    private int bonusPlots;
    private List<Plot> plots;
    private int playTimeProgress;
    private int playTimeThreshold;
    private Timestamp lastSeen;
    private PluginConfig pluginConfig;

    public UserData(PlayerPlot plugin){
        this.bonusPlots = 0;
        this.plots = new ArrayList<>();
        this.playTimeProgress = 0;
        this.pluginConfig = plugin.getPluginConfig();
        this.playTimeThreshold = pluginConfig.getThreshold();
        this.lastSeen = Timestamp.from(Instant.now());
    }

    public UserData(int bonusPlots, List<Plot> plots , int playTimeProgress, int playTimeThreshold, Timestamp lastSeen, PlayerPlot plugin){
        this.bonusPlots = bonusPlots;
        this.plots = plots;
        this.playTimeProgress = playTimeProgress;
        this.playTimeThreshold = playTimeThreshold;
        this.lastSeen = lastSeen;
        this.pluginConfig = plugin.getPluginConfig();
    }

    public int getBonusPlots(){
        return bonusPlots;
    }

    public void grantBonusPlot(){
        bonusPlots++;
    }

    public List<Plot> getPlots(){
        return plots;
    }

    public int getPlotsUsed(){
        int plotsUsed = 0;
        for(Plot plot: plots){
            plotsUsed += plot.getRelativeSize();
        }
        return plotsUsed;
    }

    public void addPlot(Plot plot){
        plots.add(plot);
    }

    public void removePlot(Plot plot){
        plots.remove(plot);
    }

    public int getPlayTimeProgress() {
        return playTimeProgress;
    }

    public void incrementTimeProgress(int seconds){
        playTimeProgress+=seconds;
    }

    public void resetPlayTimeProgress(){
        playTimeProgress = 0;
    }

    public int getPlayTimeThreshold() {
        return playTimeThreshold;
    }

    public void updateThreshold(){
        playTimeThreshold += pluginConfig.getSequentialThresholdIncrement();
        if(playTimeThreshold > pluginConfig.getMaxThresholdDefault()){
            playTimeThreshold = pluginConfig.getMaxThresholdDefault();
        }
    }

    public Timestamp getLastSeen(){
        return lastSeen;
    }

    public void setLastSeen(Timestamp time){
        this.lastSeen = time;
    }

}
