package com.eclipsekingdom.playerplot;

import org.bukkit.entity.Player;

public class Permissions {

    private static String PLOT_DEED_PERM = "playerplot.deed";
    private static String NO_LIMIT_PERM = "playerplot.nolimit";
    private static String BUILD_PERM = "playerplot.build";
    private static String ACTIVITY_PERM = "playerplot.activity";
    private static String DELETE_PERM = "playerplot.delete";
    private static String VIEW_PERM = "playerplot.view";


    public static boolean hasNoLimit(Player player){
        return hasPermission(player, NO_LIMIT_PERM);
    }

    public static boolean canSummonPlotDeed(Player player){
        return hasPermission(player, PLOT_DEED_PERM);
    }

    public static boolean canBuildEverywhere(Player player){
        return hasPermission(player, BUILD_PERM);
    }

    public static boolean canChechActivity(Player player){
        return hasPermission(player, ACTIVITY_PERM);
    }

    public static boolean canDeletePlots(Player player){
        return hasPermission(player, DELETE_PERM);
    }

    public static boolean canViewAllPlots(Player player){
        return hasPermission(player, VIEW_PERM);
    }

    private static boolean hasPermission(Player player, String permString){
        return (player.hasPermission("playerplot.*") || player.hasPermission(permString));
    }

    public static boolean hasExtraCommands(Player player){
        return (canSummonPlotDeed(player) || canChechActivity(player) || canDeletePlots(player) || canViewAllPlots(player));
    }

}
